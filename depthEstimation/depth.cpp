#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

bool showImg = false;

int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: depth <Image_Path>");
        return -1;
    }

    Mat image;
    image = imread( argv[1], CV_LOAD_IMAGE_COLOR );
    
	if ( !image.data )
    {
        printf("No image data \n");
        return -1;
    }
	String name = String(argv[1]);
    namedWindow("Display Image", WINDOW_AUTOSIZE );

	//resize
	Size size (800, (800*image.rows)/image.cols);
	resize(image, image, size);

//1 get red pixels (identified as finger tip) (this step should be adapted from previous phase) 
	Mat redPixels;
	cvtColor(image, redPixels, CV_BGR2GRAY);
	for (int i = 0; i < image.rows; ++i)
		for (int j = 0; j < image.cols; ++j){
			Vec3b color = image.at<Vec3b>(i,j);
		//	std::cout << color << std::endl;
			if (color[0] < 5 && color[1] < 5 && color[2] > 250 )//red
				redPixels.at<unsigned char>(i,j) = 255;
			else
				redPixels.at<unsigned char>(i,j) = 0;
	}
	if (showImg){
		imshow("Display Image", redPixels);
		waitKey(0);
	}
//2 get connected component corresponding to the thimble (will be input from phase 1)
//3 assume we have one blob of red pixels
//3.1 compute mean
	Vec2f m (0,0);
	int area = 0;
	for (int i = 0; i < redPixels.cols; ++i)
		for (int j = 0; j < redPixels.rows; ++j){
			if (redPixels.at<unsigned char>(j,i) == 255)
			{ 
				area+=1;
				m += Vec2f(i,j);
			}
	}
	m = m/area;
//3.2 compute covariance
	Mat c = Mat::zeros(2, 2, CV_32F);
	for (int i = 0; i < redPixels.cols; ++i)
		for (int j = 0; j < redPixels.rows; ++j){
			if (redPixels.at<unsigned char>(j,i) == 255)
			{
				Mat x = Mat(Vec2f(i,j)-m);
				Mat xt; transpose(x,xt);
				c += x*xt;
			}
	}
	c = c/area;
//3.3 compute principal axes
	Mat d, u, vt;
	SVD::compute(c,d,u,vt);
	Vec2f x1 = u.col(0); normalize(x1);
	Vec2f x2 = u.col(1); normalize(x2);
	Mat checkAxes;
	redPixels.copyTo(checkAxes);
	line (checkAxes, Point2i(m), Point2i(m+100*x1), Scalar(100), 2);		
	line (checkAxes, Point2i(m), Point2i(m+100*x2), Scalar(100), 2);		
	if (showImg){
		imshow("Display Image", checkAxes);
		waitKey(0);
	}
//4 get x' and y' width and length and draw bounding box
	float xmax = 0, ymax = 0, xmin = 0, ymin = 0;
	Vec3f p1, p2;
	for (int i = 0; i < redPixels.cols; ++i)
		for (int j = 0; j < redPixels.rows; ++j){
			if (redPixels.at<unsigned char>(j,i) == 255)
			{
				Vec2f v = Vec2f(i,j)-m;
				float x = v.dot(x1);
				float y = v.dot(x2);
				if (x < xmin)
					xmin = x;
				if (y < ymin)
				{
					ymin = y;
					p1 = Vec3f(i,j,1);
				}
				if (x > xmax)
					xmax = x;
				if (y > ymax)
				{
					ymax = y;
					//p2 = Vec3f(i,j,1);
				}
			}
	}
	float w = xmax-xmin;
	float h = ymax-ymin;
	Vec2f centroid = m + (xmax+xmin)/2.0*x1 + (ymax+ymin)/2.0*x2;
	Mat checkBox;
	redPixels.copyTo(checkBox);
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1+h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid+w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid+w/2.0*x1-h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid-w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	if (showImg){
		imshow("Display Image", checkBox);
		waitKey(0);
	}
	p2 = p1+h*Vec3f(x2[0], x2[1], 0);
//5 assume that h = 2*radius of the thimble (for the moment, could be improved if we use more colours)
	float r = 1.8; //18mm
	Mat camMat; //need from a calibration step
	camMat = Mat::eye(3,3, CV_32F); //set to identity for the moment
	Mat P1_hom = camMat.inv()*Mat(p1);
	Mat P2_hom = camMat.inv()*Mat(p2);
	//P1.z ~= P2.z
	float dist = sqrt((P1_hom-P2_hom).dot(P1_hom-P2_hom));
	Mat P1 = P1_hom*r/dist;
	Mat P2 = P2_hom*r/dist;
	std::cout << P1 << std::endl;
	std::cout << P2 << std::endl;

	return 0;
}
