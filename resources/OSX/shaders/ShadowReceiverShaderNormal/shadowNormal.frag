#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
	vec3 Tangent;
	vec3 Bitangent;
} fs_in;

uniform sampler2D diffuseMap;
uniform sampler2D depthMap;
uniform sampler2D backfaceDepthMap;
uniform sampler2D colorMap;
uniform sampler2D normalMap;

uniform vec3 LP;
uniform vec3 viewPos;

float ShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(depthMap, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
	//vec3 lightDir = normalize(LP - fs_in.FragPos);
	// vec3 normal = normalize(fs_in.Normal);
	float bias=0.000f;
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(depthMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
	 for(int y = -1; y <= 1; ++y)
	   {
			float pcfDepth = texture(depthMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= 9.0;
   // float bias = 0.005;//max(0.05 * (1.0 - dot(normal, lightDir)), 0.005); 
	//float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;  

    return shadow;
}

float BackShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(depthMap, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
	//vec3 lightDir = normalize(LP - fs_in.FragPos);
	// vec3 normal = normalize(fs_in.Normal);
	float bias=0.000f;
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(backfaceDepthMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
	 for(int y = -1; y <= 1; ++y)
	   {
			float pcfDepth = texture(backfaceDepthMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= 9.0;
   // float bias = 0.005;//max(0.05 * (1.0 - dot(normal, lightDir)), 0.005); 
	//float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;  

    return shadow;
}

float ThicknessCalculation(vec4 fragPosLightSpace)
{
 // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float backDepth = texture(backfaceDepthMap, projCoords.xy).r; 
	float frontDepth = texture(depthMap, projCoords.xy).r;
	return (backDepth-frontDepth);
	//return 0;

}
vec4 ShadowColorCalculation(vec4 fragPosLightSpace)
{
// perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    //vec4 color = texture(colorMap, projCoords.xy); 
	float bias=0.000f;
	vec4 color= vec4(0.0,0.0,0.0,0.0);
	vec2 texelSize = 1.0 / textureSize(depthMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
	 for(int y = -1; y <= 1; ++y)
	   {
			vec4 pcfColor = texture(colorMap, projCoords.xy + vec2(x, y) * texelSize); 
			color += pcfColor;      
		}    
	}
	color /= 9.0;
	return color;
}

void main()
{           
    vec3 color = texture(diffuseMap, fs_in.TexCoords).rgb;
    vec3 normal = normalize(fs_in.Normal);
    vec3 lightColor = vec3(1.0);
	vec4 shadowColor = ShadowColorCalculation( fs_in.FragPosLightSpace); 
	//
	vec4 diffuseTexel = texture(diffuseMap,var_uv);
	vec3 normalTexel = normalize(texture(normalMap,var_uv).rgb*2.0-1.0);
	vec3 light =TBN*normalize(LP- var_pos);
	vec3 eyes = TBN* normalize(-var_pos);
	vec3 halfv = normalize(light + eyes);  
	float diff= max(dot(light,halfv), 0.0);
    // Diffuse
    vec3 lightDir = normalize(LP - fs_in.FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    // Specular
    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * color;    
    // Calculate shadow
    float shadow = ShadowCalculation(fs_in.FragPosLightSpace)*BackShadowCalculation(fs_in.FragPosLightSpace);
	  // Ambient
    vec3 ambient = color*0.25;
	//diffuse = shadowColor.rgb;
    float thickness=ThicknessCalculation(fs_in.FragPosLightSpace)*100f+0.1f;
	vec3 lighting = ambient+diffuse+specular;
   // vec3 lighting =((1-shadow)*ambient+shadow*shadowColor.rgb+ (1.0 - shadow) * (diffuse + specular)+shadow*(diffuse+shadowColor.rgb*(thickness*100+0.1))) * color; 
	//FragColor = vec4(lighting,1.0);  
    //FragColor = thickness*vec4(lighting,1.0);
	//FragColor =  shadow*thickness*vec4(lighting,1.0)+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	//FragColor =  shadow*thickness*vec4(lighting,1.0)*50+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	//FragColor =  shadow*thickness*vec4(lighting,1.0)*50*shadowColor+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	FragColor =  shadow*thickness*vec4(lighting,1.0)*25*shadowColor+(1-shadow)*vec4(ambient,1)+vec4(0,0,0,1);

}