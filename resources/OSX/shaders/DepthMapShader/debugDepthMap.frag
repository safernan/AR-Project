#version 330 core
out vec4 color;
in vec2 var_uv;

uniform sampler2D depthMap;
uniform sampler2D colorMap;

void main()
{             
    float depthValue = texture(depthMap, var_uv).r;
    color = vec4(texture(colorMap, var_uv).rgb,1-texture(depthMap,var_uv).r);
}  