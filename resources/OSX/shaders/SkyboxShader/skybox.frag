#version 450

uniform samplerCube skyboxMap;

layout(location = 0) out vec4 fragment_color;

in vec3 TexCoords;
 
void main (void)  
{  
    fragment_color = texture(skyboxMap,TexCoords);    
}