#version 450

uniform mat4 MVPNoTranslation;
layout(location = 0) in vec3 pos;

out vec3 TexCoords;
void main(void)  
{     
  gl_Position = MVPNoTranslation * vec4(pos,1); 
  TexCoords = pos;
}