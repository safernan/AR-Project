#version 450


uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform vec3 LP;

uniform samplerCube skyboxMap;

uniform float alphaValue;

in vec3 var_pos;
in vec3 var_normal;
in vec2 var_uv;
in vec3 var_tangent;
in vec3 var_bitangent;

layout(location = 0) out vec4 fragment_color;

const float etaR = 0.95;
const float etaG = 0.96;
const float etaB = 0.97;

void main (void){
    mat3 TBN = transpose(mat3(var_tangent,var_bitangent,var_normal));
	vec3 normalTexel = TBN*normalize(texture(normalMap,var_uv).rgb*2.0-1.0);
	vec3 refractionR = refract(normalize(var_pos),normalize(normalTexel),etaR);
	vec3 refractionG = refract(normalize(var_pos),normalize(normalTexel),etaG);
	vec3 refractionB = refract(normalize(var_pos),normalize(normalTexel),etaB);
	vec4 refractionColor;
	refractionColor.r = texture(skyboxMap,refractionR).r;
	refractionColor.g = texture(skyboxMap,refractionG).g;
	refractionColor.b = texture(skyboxMap,refractionB).b;
	//refractionColor.a =1;
	fragment_color = vec4(refractionColor.rgb,alphaValue);
}