#version 450


uniform sampler2D diffuseMap;

uniform float alphaValue;

layout(location = 0) out vec4 fragment_color;

in vec2 var_uv;

void main (void){
 	vec4 diffuseTexel = texture(diffuseMap,var_uv);
	fragment_color = vec4(diffuseTexel.rgb,alphaValue);
}