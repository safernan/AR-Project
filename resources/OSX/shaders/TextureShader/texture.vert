#version 450

uniform mat4 MVP;

layout(location = 0) in vec3 pos;
layout(location = 2) in vec2 uv;

out vec2 var_uv;

void main(void){
  gl_Position = MVP * vec4(pos.x,pos.y,pos.z,1);
  var_uv.x =uv.x; var_uv.y=1.0-uv.y;
  //if(var_uv.x<0)var_uv+=1;
  //if(var_uv.y<0)var_uv+=1;
 }