#version 450

uniform mat4 MV;
uniform mat4 MVP;
uniform mat3 MV3X3;
uniform mat4 M;
uniform mat4 lightSpaceMatrix;
uniform mat4 lightView;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

out vec3 var_pos;
out vec3 var_normal;
out vec2 var_uv;
out vec3 var_tangent;
out vec3 var_bitangent;

void main(void){
  gl_Position = lightSpaceMatrix * M * vec4(pos, 1.0f);
  var_pos = (lightView*M* vec4(pos.x,pos.y,pos.z,1)).xyz;
  var_normal = ((lightView*M)*vec4(normalize(normal),0)).xyz;
  var_uv.x = uv.x; var_uv.y=1.0-uv.y;
  var_tangent = ((lightView*M)*vec4(normalize(tangent),0)).xyz;
  var_bitangent = ((lightView*M)*vec4(normalize(bitangent),0)).xyz;
}