varying vec3 normal,view;  
uniform sampler2D myTexture;
varying vec2 vTexCoord;
void main (void)  
{  
   vec3 light = normalize(gl_LightSource[0].position.xyz - view);   
   vec3 eyes = normalize(-view);  
   vec3 refl = normalize(-reflect(light,normal));      
   vec4 diff = max(dot(normal,light), 0.0);   
   vec4 spec = pow(max(dot(refl,eyes),0.0),100); 

   gl_FragColor =clamp(vec4(0,0,1,1)*texture2D(myTexture,vTexCoord),0.0,1.0)+ clamp(diff*texture2D(myTexture,vTexCoord),0.0,1.0)+clamp(spec*vec4(1,1,1,1),0.0,1.0);     
}