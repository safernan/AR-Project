varying vec3 normal,view;  
varying vec2 vTexCoord;
uniform sampler2D myTexture;  
void main (void)  
{ 
   normal = normalize(normal);
   vec3 light = normalize(gl_LightSource[0].position.xyz - view);
    vec3 eyes =  normalize(-view); 
   vec3 half = normalize(light + eyes);  
   float diff= max(dot(light,normal), 0.0);
   float spec=0.0;
   if(diff>0.0)
	spec = ceil(pow(max(dot(half, normal), 0.0),1000)*3)/3;   
   gl_FragColor = vec4(0,0,1,1)*ceil(0.25*9)/9+ clamp(ceil(diff*texture2D(myTexture,vTexCoord)*5)/5,0.0,1.0)+clamp(spec*vec4(1,1,1,1),0.0,1.0);     
}