void main()
{	
	vec3 normal = normalize(gl_NormalMatrix * gl_Normal);		
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_Position = gl_Position+vec4(normal[0],normal[1],normal[2],0)*0.05+vec4(0,0,0.1,0);
}
