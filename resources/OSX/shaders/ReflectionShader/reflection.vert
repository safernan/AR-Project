#version 450

uniform mat4 M;
uniform mat4 MVP;
uniform mat4 NM;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;

out vec3 Reflect;

void main(void){
  gl_Position = MVP * vec4(pos,1);
  vec3 I = ((M*vec4(pos,1)).xyz);
  Reflect = reflect(I,normalize((NM*vec4(normal,0)).xyz));
}