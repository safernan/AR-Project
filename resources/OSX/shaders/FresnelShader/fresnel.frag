#version 450

uniform samplerCube skyboxMap;

uniform float alphaValue;

layout(location = 0) out vec4 fragment_color;

in vec3 Reflect;
in vec3 RefractR,RefractG,RefractB;
in float Ratio;

void main (void){
	vec3 refractColor;
	refractColor.r = vec3(texture(skyboxMap, RefractR)).r;
	refractColor.g = vec3(texture(skyboxMap, RefractG)).g;
	refractColor.b = vec3(texture(skyboxMap, RefractB)).b;
	vec3 reflectColor = vec3(texture(skyboxMap, Reflect));
	vec3 color = mix(refractColor, reflectColor,Ratio);
	fragment_color = vec4(color,alphaValue);
}