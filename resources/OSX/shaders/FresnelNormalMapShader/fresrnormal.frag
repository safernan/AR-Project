#version 450


uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform vec3 LP;
uniform mat4 NM;

uniform samplerCube skyboxMap;

uniform float alphaValue;

in vec3 var_pos;
in vec3 var_normal;
in vec2 var_uv;
in vec3 var_tangent;
in vec3 var_bitangent;

layout(location = 0) out vec4 fragment_color;

const float EtaR = 0.95;
const float EtaG = 0.96;
const float EtaB = 0.97;

const float fresnelPower = 5.0;
const float F = ((1.0-EtaG) * (1.0-EtaG)) / ((1.0+EtaG) * (1.0+EtaG));
float Ratio;
void main (void){
    mat3 TBN = (transpose(mat3(var_tangent,var_bitangent,var_normal)));
	vec3 normalTexel = TBN*normalize(texture(normalMap,var_uv).rgb*2.0-1.0);
	vec3 refractionR = refract(normalize(var_pos),normalize(normalTexel),EtaR);
	vec3 refractionG = refract(normalize(var_pos),normalize(normalTexel),EtaG);
	vec3 refractionB = refract(normalize(var_pos),normalize(normalTexel),EtaB);
	vec3 reflection = reflect(normalize(var_pos),normalize(normalTexel));
	vec4 reflectionColor = texture(skyboxMap,reflection);
	vec4 refractionColor;
	refractionColor.r = texture(skyboxMap,refractionR).r;
	refractionColor.g = texture(skyboxMap,refractionG).g;
	refractionColor.b = texture(skyboxMap,refractionB).b;
	Ratio = F+(1.0-F)*pow(1.0-dot(normalize(-normalize(var_pos)),normalize((vec4(normalTexel,0)).xyz)),fresnelPower);
	//Ratio = clamp(Ratio,0,1);
	vec4 color = mix(refractionColor, reflectionColor,Ratio);
	fragment_color = vec4(color.rgb,alphaValue);
}