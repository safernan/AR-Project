#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include "backProjection.h"
#include "depthEstimation.h"

using namespace std;
using namespace cv;


int mainnn(int argc, char** argv) {
    computeHistogram();
    

    VideoCapture cap(0);
    if (argc > 1){
	char* videoFilename = argv[1];
	cap = VideoCapture(videoFilename);
	}

    
    while(cv::waitKey(30) != 27) {  
        Mat frame;
        cap >> frame;
	Size newSize(639,frame.rows*639/frame.cols);
	resize(frame,frame,newSize);
        if(frame.empty()) return -1;
	Mat contourImage = backproject(frame);
        imshow("frame", contourImage);
	Mat p = depth(contourImage, cv::Mat::eye (4, 4, CV_32F), cv::Mat::eye (4, 4, CV_32F));
	std::cout<< p << std::endl;
    }
    return 0;
}
