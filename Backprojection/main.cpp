#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int bins = 55;
int histSize[] = { bins, bins };
int channels[] = { 0, 0 };
float h_ranges[] = { 0, 180 };
float s_ranges[] = { 0, 256 };
float v_ranges[] = { 0, 256 };
const float* ranges[] = { h_ranges, s_ranges, v_ranges };
MatND hist;

Mat hsvVid, hue, backproject, ROI;

int main(int argc, char** argv) {
    Mat src, hsv, hue;
    src = imread("blueCones.jpg");
    cvtColor(src, hsv, CV_BGR2HSV);
    calcHist(&hsv, 1, channels, Mat(), hist, 2, histSize, ranges);
    normalize(hist, hist, 1.0);
    

    VideoCapture cap(0);
    if (argc > 1){
	char* videoFilename = argv[1];
	cap = VideoCapture(videoFilename);
	}
    namedWindow("Back projection",1);

    Size S = Size((int) cap.get(CAP_PROP_FRAME_WIDTH),    
                  (int) cap.get(CAP_PROP_FRAME_HEIGHT));
	int ex = static_cast<int>(cap.get(CAP_PROP_FOURCC));
  VideoWriter writer ("capture.avi", ex, 30, cvSize(  639, 500 ) );
bool opened = false;  

    
    while(cv::waitKey(30) != 27) {  
        Mat frame;
        cap >> frame;
        if(frame.empty()) return -1;
	Size newSize(639,frame.rows*639/frame.cols);
	resize(frame,frame,newSize);
        flip(frame, frame, 1);
        cvtColor(frame, hsvVid, CV_BGR2HSV);

	if (!opened)
	{
		opened = true;	    
		writer.open("capture.avi", ex, cap.get(CAP_PROP_FPS), newSize, true);
	}        
        hue.create( hsvVid.size(), hsvVid.depth() );
        mixChannels( &hsvVid, 1, &hue, 1, channels, 1 );
        
        calcBackProject(&hue, 1, channels, hist, backproject, ranges, 255.0);

        for (int i = 1; i < 3; i = i + 2)
            GaussianBlur(backproject, backproject, Size(i, i), 0, 0);
        threshold(backproject, ROI, 50, 255, CV_THRESH_BINARY);

        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        vector<int> selected;
        // Find contours
        findContours(ROI, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

        Mat contourImage(ROI.size(), CV_8UC1, cv::Scalar(0));
        for (int idx = 0; idx < contours.size(); idx++) {
            Rect r = boundingRect(contours[idx]);
            if(r.area() > 1200)
            {
		selected.push_back(idx);
                drawContours(contourImage, contours, (int)idx, Scalar(255), FILLED, 8, vector<Vec4i>(), 0, Point());
	    }
        }
        /*for (size_t i = 0; i < selected.size(); i++) {
            rectangle(frame, boundingRect(contours[selected[i]]), Scalar(0, 0, 255), 5);
        }*/
        imshow("frame", contourImage);
//3.1 compute mean	
	bool showImg = false;
	Vec2f m (0,0);
	int area = 0;
	for (int i = 0; i < contourImage.cols; ++i)
		for (int j = 0; j < contourImage.rows; ++j){
			if (contourImage.at<unsigned char>(j,i) == 255)
			{ 
				area+=1;
				m += Vec2f(i,j);
			}
	}
	m = m/(float)area;
//3.2 compute covariance
	Mat c = Mat::zeros(2, 2, CV_32F);
	for (int i = 0; i < contourImage.cols; ++i)
		for (int j = 0; j < contourImage.rows; ++j){
			if (contourImage.at<unsigned char>(j,i) == 255)
			{
				Mat x = Mat(Vec2f(i,j)-m);
				Mat xt; transpose(x,xt);
				c += x*xt;
			}
	}
	c = c/area;
//3.3 compute principal axes
	Mat d, u, vt;
	SVD::compute(c,d,u,vt);
	Vec2f x1 = u.col(0); normalize(x1);
	Vec2f x2 = u.col(1); normalize(x2);
	Mat checkAxes;
	contourImage.copyTo(checkAxes);
	line (checkAxes, Point2i(m), Point2i(m+100*x1), Scalar(100), 2);		
	line (checkAxes, Point2i(m), Point2i(m+100*x2), Scalar(100), 2);		
	if (showImg){
		imshow("axes", checkAxes);
		//waitKey(0);
	}
//4 get x' and y' width and length and draw bounding box
	float xmax = 0, ymax = 0, xmin = 0, ymin = 0;
	Vec3f p1, p2;
	for (int i = 0; i < contourImage.cols; ++i)
		for (int j = 0; j < contourImage.rows; ++j){
			if (contourImage.at<unsigned char>(j,i) == 255)
			{
				Vec2f v = Vec2f(i,j)-m;
				float x = v.dot(x1);
				float y = v.dot(x2);
				if (x < xmin)
					xmin = x;
				if (y < ymin)
				{
					ymin = y;
					p1 = Vec3f(i,j,1);
				}
				if (x > xmax)
					xmax = x;
				if (y > ymax)
				{
					ymax = y;
					//p2 = Vec3f(i,j,1);
				}
			}
	}
	float w = xmax-xmin;
	float h = ymax-ymin;
	Vec2f centroid = m + (xmax+xmin)/2.0*x1 + (ymax+ymin)/2.0*x2;
	Mat checkBox;
	frame.copyTo(checkBox);
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1+h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid+w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid+w/2.0*x1-h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	line (checkBox, Point2i(centroid-w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);		
	if (true || showImg){
		imshow("box", checkBox);
		writer << checkBox;
		//writer.write(checkBox);
		//waitKey(0);
	}
	p2 = p1+h*Vec3f(x2[0], x2[1], 0);
//5 assume that h = 2*radius of the thimble (for the moment, could be improved if we use more colours)
	float r = 1.8; //18mm
	Mat camMat; //need from a calibration step
	camMat = Mat::eye(3,3, CV_32F); //set to identity for the moment
	Mat P1_hom = camMat.inv()*Mat(p1);
	Mat P2_hom = camMat.inv()*Mat(p2);
	//P1.z ~= P2.z
	float dist = sqrt((P1_hom-P2_hom).dot(P1_hom-P2_hom));
	Mat P1 = P1_hom*r/dist;
	Mat P2 = P2_hom*r/dist;
	std::cout << P1 << std::endl;
	std::cout << P2 << std::endl;
    }
    return 0;
}
