\documentclass[10pt]{article}
\usepackage[british,UKenglish,USenglish,english,american]{babel}
\usepackage{amsfonts}
\usepackage[12pt]{extsizes}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{algorithm}
\usepackage{graphicx}
\usepackage[noend]{algpseudocode}
\usepackage[left=3cm,right=3cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{multicol} 
\usepackage{hyperref}
\usepackage[nottoc, notlof, notlot]{tocbibind}

\usepackage{enumerate}

\title{Augmented Reality\\ Report \\Augmented 3D painting}
\date{\today}
\author{Sean \textsc{Dillon} Sarah \textsc{Fernandes-Pinto-Fachada}\\ Eduardo \textsc{Mu\~noz} Jian Zheng \textsc{Ooi}}



\begin{document}

\maketitle

\section{Introduction}
Augmented reality has advanced significantly during the last years, with a lot of focus on producing hardware that gives very accurate information as input to the frontend software. Products such as Microsoft Hololens\cite{hololens} and the Google Tango\cite{tango} project are good examples of this. The current market prices of such devices though, make good quality AR a very expensive means of entertainment. In addition to their price, we feel that wearing AR equipment on a daily basis is a new habit for potential users of AR technology, which may not be received well.

From what we have seen so far, most developers think AR mostly as an entertainment or presentational tool. While we agree that Augmented Reality offers a new dimension to the entertainment industry, we think that it can be used to solve existing problems in the 3D design industry; from our experience with modeling tools, we have detected that the traditional means of interaction, keyboard and mouse, can be cumbersome when working for such a task.

For the reasons mentioned above, we decided to work on an interactive application that requires minimum external hardware and no wearable hardware if possible. The ultimate purpose of the application is to allow the user to design his 3D models using the most natural and intuitive communication method: their hands. The original plan was to create a sculpting application for 3D models, but due to time constraints and foreseen challenges, we decided to reduce our project to a simpler problem, a painting application. Our vision for the future of our work is an AR plugin for design in popular modeling software.

\section{Theoritical background}
This project is the integration of many concepts learned along different courses, mainly from: Augmented Reality, Computer Vision, Computer Graphics and Computational Physics. For a deeper understanding on the pipeline of this interactive application we are going to define some concepts that were implemented in our work.

\paragraph{Color backprojection:} It is used for image segmentation, based on a histogram of a sample color, we back-project it over the test image to find the object of interest. In other words, we calculate the probability of every pixel being the sample color of the histogram, the output is a grayscale image where white means a perfect match and black as not match at all. With this method we can find a sample color in an image. Thresholding the output will give us an area of the color found in the image. 

\paragraph{Transformation matrix:} is a special transformation that can describe 2D and 3D transformations, and are easily represented, combined and computed. A transformation matrix can hold the information of position, scaling and rotation to be pass to 3D model points.

\paragraph{Collision detection:} is a procedure in computational physics where given some geometrical entities (eg meshes, triangles, points or planes) we detect when two entities physically collide in space, and find the points of collision as well as the entities that collided.

\paragraph{Splashing:} is a volume rendering algorithm. While there is more to the technique, here we mention splashing as the case where a kernel alpha image is used, and a color is distributed around a point on an image according to the alpha values of the kernel.


\section{Implementation}
The code program that was implemented follows a series of task. Please see below the general process that is being done when running the program, shown in figure \ref{diag}.

\begin{figure}[h]
\begin{center}
\includegraphics[width=7cm]{diag2.png}
\caption{\label{diag} Process of AR Painter application.
}
\end{center}
\end{figure}

\subsection{Rendering the 3D model}
The base of our application is the SimpleLite project from ARToolKit
\cite{artoolkit}. SimpleLite is a program that makes use of the camera to get real time video. In each frame the code is looking for the physical Hiro pattern and if it is found, the code renders a 3D cube on top of the pattern. For our application we change the 3D cube to be an wavefront (.obj) model that we loaded dynamically. To do so, we had to integrate ARToolkit libraries with OpenGL.

The SimpleLite code detects the marker and then it calculates the camera transformation matrix. This matrix contains the position, scaling and rotation of the 3D virtual object, in other words depending on the position of the marker the visualization of the 3D cube will be different: the cube will be always on the top of the marker. We made use of this camera transformation matrix to render the virtual sphere. We removed the code rendering the cube and change it for a Sphere using OpenGL, shaders and textures. The sphere is rendered in top of the marker and it shows a grid texture to observe the rotation of the virtual model depending on the marker position, as shown in figure \ref{marker}.


\begin{figure}[h]
\begin{center}
\includegraphics[width=9cm]{hiropattern.png}
\caption{\label{marker} 3D Sphere rendered in top of the marker.
}
\end{center}
\end{figure}

\subsection{Finding the thimble}
In each frame image we make a backprojection using a yellow color sample to find the thimble. The thimble represent the tip of our finger which will paint the virtual Sphere. To perform a backprojection we include OpenCV\cite{opencv} into the code. For better results we used pictures of the actual thimble to make a sample of the actual color that we want to find. The histogram is calculated at the beginning and stored in a global variable and then it is used in the main loop to perform the backprojection at every frame. 

Once the thimble is found we threshold the output to have an area of the thimble and then we draw a bounding box around it. 


\subsection{Calculate the thimble position}
In order to get the position of the thimble, we assume it is a cylinder of known radius $r$ and of height $h>r$. From every point of view we look at the cylinder, the oriented bounding box width or height corresponds to the projection of distance $r$ on a line parallel to the camera plane. Moreover, it is always the smallest value which corresponds to $r$.

The thimble is not a perfect cylinder, and from some point of view, the finger can occlude the bottom part. However, that particular case rarely happens,  and that approximation demonstrated to work well.
So, as a first step, we compute the oriented bounding box that surrounds the white pixels given by the backprojection (figure \ref{thimble}). The quality of this step highly depends of the accuracy of backprojection.

\begin{figure}[h]
\begin{center}
\includegraphics[width=9cm]{thimble.png}
\caption{\label{thimble} Thimble detection and bounding box to calculate the 3D location.
}
\end{center}
\end{figure}

Then, we find two 2D points, $p_1$ and $p_2$, with are at same height, and diametrically opposed. We know that those two points are in a plane parallel to the camera plane, and they are at a distance r in 3D. We also know the distance between $p_1$ and $p_2$ in pixels. Those information allow us to deduce the 3D position $P_1$ and $P_2$. The tip of the finger is set as the middle point between $P_1$ and $P_2$.
In practice, those point are at the bottom of the thimble, due to the round shape at the other side (I will try to improve that this afternoon).The transformation matrix of the thimble is then obtained. There is no rotation, just a translation, and we display a sphere at that location. This matrix needs to be in the same coordinate system as the 3D model we paint, which is defined by the size of the AR pattern we printed.


Some false positive can occur, and dramatically change the shape of the bounding box. Nevertheless, as they are often sparse pixels, it is easy to detect and suppress them, if not too numerous. We divide the bounding box in two according to the largest side, and check the number of pixels each side. If the ratio is clearly disproportionate, we suppress the pixels of the less numerous side. Then we recompute the bounding box. We limit the number of iteration of that process to 3, to avoid losing too much time on this step.


\subsection{Get collision location if any}
The thimble position is a point in 3D space. To check of the finger is touching the sphere, we check for a collision between the sphere and the thimble position. Our approach is to iterate over all the triangles of the model and for each triangle, calculate its normal vector by averaging the normals of the corresponding vertices. The result must be transformed by the model matrix of the object since we need to work in global space. 

In practice we transform the query point instead, by the inverse of the model transform. This is done once and avoids some computational cost. Given the normal and the three positions of the vertices, we essentially define a plane and we check if the point is inside the plane. This is done by creating two vectors and comparing their angle. The first vector starts from the defining point of the plane and ends at the query point. The second is the normal vector of the plane. By normalising the first vector and by taking the dot product of the two vectors, we get the cosine of the angle between them. If the cosine is zero or negative, the angle is equal or bigger than 90 degrees and thus the query point is inside the plane. Otherwise, it is outside. A collision is detected when the query point is inside the plane and its distance from the plane is under a user-defined threshold.


\subsection{Show collision in the render sphere}
If a collision is detected, we need to paint the model texture at that position. This is done in three steps. The first step is to find the uv coordinates of the collision point. By holding a reference to the colliding triangle, we interpolate the collision point using the positions of the three vertices and their UV coordinates using barycentric interpolation. In the second step we use the alpha image of a brush (in the demo we use a gaussian distribution kernel) and we splash the painting color using that image. The pixel that we splash around is found by scaling the interpolated UV coordinates by the dimensions of the image. The size of the splash depends on the resolution of the kernel image and the resolution of the texture image. These have a fixed size; a high resolution texture fits all models. In the last step, we reupload the texture to the GPU so the result is visible in real time.
\begin{figure}[h]
\begin{center}
\includegraphics[width=6cm]{screen1.png}
\includegraphics[width=6cm]{screen2.png}
\caption{\label{collision} Rendering collision, hand segmentation and painting.
}
\end{center}
\end{figure}

\subsection{Segment the hand in the image}
The hand is segmented from the image since we need to render the hand on top of the graphics in some instances, when the hand is between the 3D model and the camera.

The initial approach was to wear a yellow glove and backproject the color. After some experimentation we ended up with using foreground detection since the hand is moving during the application.

\subsection{Check if hand should be rendered on top of graphics}
After the hand mask has been derived, we must render the hand on top of the frame, but only if this would be true if the artificial model was physical. To decide when this happens, we use the finger position and during the collision detection phase we find its nearest point on the model. We calculate this with a point-to-triangle distance function and keep the normal of the triangle that has the minimum distance. We then transform the normal to the global space and check the angle with the viewer’s forward vector. If the angle is bigger than 90 degrees the hand is rendered on top of the graphics, otherwise it is not. 

By assigning the default state as graphics-over-hand, we cover the case where the finger is inside the model and thus it has no nearest neighbor on the model surface. While this works well on our convex shape (sphere) used in the demo, the same approach is reasonable for concave objects: Consider the case of a horizontal torus. If the finger is inside the torus, the torus is rendered on top of the hand and if the finger is on the hole region of the torus, the occlusion will be decided according to the part of the surface that it is closest to, which is a reasonable behaviour. To allow the user to see what he draws when his hand is on top of the graphics, we blend the hand image with the model.

\section{Experiments, results}
During developing the project we made some experimentation to improve the performance of our application, below are the description of some of them.

\subsection{Backprojection}
In order to detect the tip of the finger we use a thimble of a single color. The first one we tried was blue.  We discover that blue color works well if there is no black nor white colors in the scene, but with lights and shadows that is very difficult. In our experiments we were picking blue colors when the background was white. Then we paint the thimble red and make some tracking experiments. We also had problems with red, we were picking some red point in our skin, as well as in the brown table where the pattern was located. Finally we changed the thimble to yellow, the same color as the latex yellow gloves that we used to make the hand segmentation that worked very well. This color worked magnificently with a white background and a dark surface. For this reason we disregard the hand segmentation and rendering we already had, so that we can use the yellow thimble instead.

Beside the color of the sample, the distance to the camera and the lighting condition play an important role in quality of detection of the finger. The depth estimation of the thimble is surprisingly accurate for such a little thing to detect. Of course that accuracy decreases if we work far away from the camera, or in bad lighting conditions. In that case, the sphere positioned at the tip of the finger flickers, and the application becomes difficult to use. 


\subsection{Pattern detection}
The 3D virtual object depends on the physical pattern being detected by the camera. If the pattern is perpendicular to the camera or being tilt to, the virtual object would disappear suddenly. Also if the hand or other object obstruct the pattern or even a tiny part of the pattern, the program won’t detect the marker and thus the virtual object is no longer rendered in the screen. To avoid such things, we change the code, so when the pattern is not visible anymore it will stay in the last location rendered until the fully pattern is shown again. This enhancement resulted in a smoother experience.

\subsection{Collision detection}
During the collision detection phase, we run into the following issue; due to low frame rate or fast movements from the user, a collision may not be detected since the point may be too far from the plane during all the frames of the hand movement. To battle this, we adjusted the distance threshold to bigger values. As a result, we had many triangles that were candidates for a collision with the point, in the same frame. Our initial approach was to select the colliding triangle whose corresponding plane had the minimum distance with the query point, using a min/max algorithm. Later we had the idea to consider only the visible triangles. This was done by comparing the normals of the triangles with the global backwards vector (0,0,1). By taking the dot product and checking the sign, we can discard the triangles that their normal has an angle bigger than 90 degrees with the viewer’s backward vector. In the end, we use both improvements for the final result. 

\subsection{Foreground detection}
Since we had experimented a lot with back projection in different colors and for different objects, we realised that back projecting and removing a green background would help all the segmentation steps. This approach carried all the lighting conditions problems we had with back projection so we tried to use foreground detection, which gave better results and can be used in an arbitrary (but static) background. Nevertheless, we still use the green background, because it is not detected after fast lightning changes the make the background need to be recomputed. 

\subsection{Movement speed and flickering}
After achieving a good result in terms of finger position estimation, we realised that we had unavoidable small flickering of the position, even when the finger would stay still. We came up with the idea of slowing down the speed of the movement of the finger. To achieve this, we calculate the direction vector using the position of the finger in the previous frame and the estimated position in the new frame. We then calculate the new position as the projection of the old position on the direction vector, multiplied by a scalar that we call speed. The speed has a maximum cap which is the original distance between the old and the new position. While this resulted a stable movement of the finger position, it was impractical since we needed a higher speed in some instances. We observed that the speed should be proportional to the minimum distance of the finger from the model; when very close to the model, the finger moves slowly to create precise drawings, otherwise it should move close to the normal speed.


\section{Conclusions}
\subsection{Application conclusions}
\begin{figure}[h]
\begin{center}
\includegraphics[width=6cm]{football.png}
\caption{\label{result} A drawing made by the application}
\end{center}
\end{figure}
The final application is working pretty well regarding the material we have, in controlled conditions. The use of green background, and thimble of of known color allows to eliminate problems of normal environment. 

However, we are deceived by the difficulty to control the drawing. Painting on such a little surface requires high precision, as feedback is given by a camera looking in a different direction than the user. The low precision of pose estimation increases the challenge of controlling the painting. 

We think we reached the best level of accuracy we can with only low cost devices and simple vision techniques. Use of depth sensor such as kinect\cite{kinect} would facilitate the localisation of the whole hand, and so the finger position estimation, and the rendering of the hand over the model, when it is between the model and the camera. 

On the other hand, the models we try to paint are about 10 centimeters large. Setting the application in a larger area could allow the user bigger movements, and so higher accuracy and control.


\subsection{Technical conclusions}
The most difficult part of our work was, without a doubt the integration. We had a lot of difficulties to compile ARToolKit, OpenGL, OpenCV among other libraries into one single program, and at the same time using different operating systems. We had to abandon windows compatibility very soon which restricted us from using the Kinect for capturing a depth map.

We feel that AR Toolkit was a bad platform to develop on. Apart from the pattern detection and parameters estimation, we had no other benefit from using this framework. On the other hand, the library enforced its own version and of OpenCV and OpenGL as well as the glut framework (instead of glfw for instance), which created a lot of frustration and loss of time. The worst feature was the lack of support for the latest Visual Studio compilers. This is a big problem since two major hardware for AR applications, the Kinect and the Hololens are Microsoft products. Finally, the code structure in the examples was chaotic. A lot of code that should have been hidden was exposed and it encouraged bad practices from our side.

Overall, there were many technical difficulties during the development. We are happy that not only we overcame these but also we managed to refine the interactive part of the application according to what we would experience from using the application.

\subsection{Future work}
This effort avoids using a depth device. We believe that using Kinect could be a good next step, since contouring on the depth map could give us a better segmentation of the hand and thimble. We also think that the best point of view of the camera would be the same as the user eyes and thus a Hololens headset could be a great addition to the application. The work that was done here still applies even when the rig would change; we have found some important issues for the physics of hand interaction applications and we have invented some optimisations that are specific to our colouring application. Our constraining factor for experimenting with this hardware, apart from lack of time, was the windows compatibility issues that we run into as well as not having Windows 10 in our lab machines.

The current demo works on a simple convex model. One thing that we considered early in the development was the geometric complexity of the models, since that might cause our collision detection to become a bottleneck. One answer is the implementation of spatial hashing. We believe that for this application, spatial hashing is ideal since the object does not change in its local space and we can always hash the query point, after it has been transformed to to the local space of the model. Since the model is manifold, an octree for spatial subdivision could also be used, in interest of memory consumption. While this would offer a logarithmic time complexity versus the fixed time complexity of spatial hashing, it deals nicely with non-uniform mesh resolutions.


\bibliographystyle{plain-fr}
\bibliography{biblio}

\end{document}
