#pragma once
//#include<Windows.h>
#include <string>
#include "debug.h"
#include <iostream>

typedef const wchar_t* LPCWSTR; //for ubuntu

void printConsole(std::string& str)
{
	std::wstring wstr = std::wstring(str.begin(), str.end());
	LPCWSTR lpcwstr = wstr.c_str();
	//OutputDebugString(lpcwstr);
	std::cout << str << std::endl;
}

void debugShader(GLuint shader) {
	int maxLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	std::string errorLog; errorLog.resize(maxLength);
	glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);
	printConsole(errorLog);
}

