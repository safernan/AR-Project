#include <cmath>
#include <glm\gtx\string_cast.hpp>
#include<glm\geometric.hpp>
#include <glm\glm.hpp>
#include <iostream>

#include "CollisionDetection.h"

#define COLLISION_EPSILON 0.1

float distance::point2point(glm::vec3 p0, glm::vec3 p1) {
	return std::sqrt((p0.x - p1.x)*(p0.x - p1.x) + (p0.y - p1.y)*(p0.y - p1.y) + (p0.z - p1.z)*(p0.z - p1.z));
}

float distance::point2edge(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2) {
	//In my understanding we need only the d1, the d2 and d3 will always be >=d1 but I follow the slides
	float d1 = glm::length(glm::cross((p2 - p1), (p0 - p1))) / glm::length(p2 - p1);
	float d2 = glm::length(p0 - p1);
	float d3 = glm::length(p0 - p2);

	return (glm::min)(d1, (glm::min)(d2, d3));
}

float distance::point2plane(glm::vec3 p0, glm::vec3 normal, glm::vec3 p1) {
	return (glm::dot((p1 - p0), normal));
}

glm::vec3 feature::point2triangle(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, feature::FEATURE& feat) {
	//check if the p0 lies in the voronoi region of the point
	//p1
	if (glm::dot((p0 - p1), (p2 - p1)) <= 0 && glm::dot((p0 - p1), (p3 - p1)) <= 0) {
		feat = feature::FEATURE::P1;
		return p1;
	}
	else if (glm::dot((p0 - p2), (p1 - p2)) <= 0 && glm::dot((p0 - p2), (p3 - p2)) <= 0) {
		feat = feature::FEATURE::P2;
		return p2;
	}
	else if (glm::dot((p0 - p3), (p1 - p3)) <= 0 && glm::dot((p0 - p3), (p2 - p3)) <= 0) {
		feat = feature::FEATURE::P3;
		return p3;
	}
	//check if the p0 lies on the voronoi region of the edges
	//p1 p2
	else if (glm::dot(glm::cross(glm::cross(p3 - p2, p1 - p2), p1 - p2), (p0 - p2)) >= 0
		&& glm::dot(p0 - p1, p2 - p1) >= 0
		&& glm::dot(p0 - p2, p1 - p2) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p2 - p1);
		glm::vec3 pe = p1 + glm::dot(p0 - p1, u_d)*u_d;
		//std::cout << "p2 is " << glm::to_string(p2) << " p1 is " << glm::to_string(p1) << " and " << glm::to_string(pe) << std::endl;
		feat = feature::FEATURE::P12;
		return pe;
	}
	//p2-p3
	else if (glm::dot(glm::cross(glm::cross(p1 - p3, p2 - p3), p2 - p3), (p0 - p3)) >= 0
		&& glm::dot(p0 - p2, p3 - p2) >= 0
		&& glm::dot(p0 - p3, p2 - p3) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p3 - p2);
		glm::vec3 pe = p2 + glm::dot(p0 - p2, u_d)*u_d;
		feat = feature::FEATURE::P23;
		return pe;
	}
	//p3-p1
	else if (glm::dot(glm::cross(glm::cross(p2 - p1, p3 - p1), p3 - p1), (p0 - p1)) >= 0
		&& glm::dot(p0 - p3, p1 - p3) >= 0
		&& glm::dot(p0 - p1, p3 - p1) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p1 - p3);
		glm::vec3 pe = p3 + glm::dot(p0 - p3, u_d)*u_d;
		feat = feature::FEATURE::P31;
		return pe;
	}
	//it is the triangle inner area
	else {
		glm::vec3 n = glm::normalize(glm::cross((p2 - p1), (p3 - p1)));
		glm::vec3 pf = p0 - glm::dot((p0 - p1), n)*n;
		feat = feature::FEATURE::P123;
		return pf;
	}
}

bool collision::point2model(glm::vec3& p, model& mod, contact* c) {
	//@TODO use bounding sphere chech as a proxy
	//iterate through each triangle of the model
	for (int i = 0; i < mod.vertices.size(); i += 3) {
		//check if the plane where the triangle is lying on is close enough to the point
		glm::vec3 normal = (mod.normals[i] + mod.normals[i + 1] + mod.normals[i + 2])*0.3333333333333f;
		if (glm::abs(distance::point2plane(mod.vertices[i], normal, p)) < COLLISION_EPSILON) {
			//check if the feature is a triangle
			feature::FEATURE feat;
			glm::vec3 contactPoint=feature::point2triangle(p, mod.vertices[i], mod.vertices[i + 1], mod.vertices[i + 2],feat);
			if (feat == feature::P123) {
				c->contactPoint = contactPoint;
				c->triangle = glm::ivec3(i, i + 1, i + 2);
				c->mod = &mod;
				return true;
			}
		}
	}
	return false;
}