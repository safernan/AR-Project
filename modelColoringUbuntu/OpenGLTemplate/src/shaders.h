#pragma once
#include <tuple>
#include <vector>
#include <string>
#include <GL/glew.h>
namespace shaders{
	void loadShaders(std::vector<std::tuple<std::string,std::string>> shaderPaths,std::vector<GLuint>& shaderIDs);

};