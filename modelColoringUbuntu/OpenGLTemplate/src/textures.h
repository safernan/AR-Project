#pragma once
#include<GL/freeglut.h>
#include <string>
#include <vector>
namespace textures {
	GLuint loadCubemap(std::vector<std::string> face);
	GLuint loadTextureDDS(std::string path);
};