#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "material.h"
struct model {
	//data buffers
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> UVs;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	//data points
	glm::vec3 centroid;
	//data memory flags
	GLuint verticesID=-1;
	GLuint normalsID=-1;
	GLuint UVsID=-1;
	GLuint tangentsID=-1;
	GLuint bitangetsID=-1;
	void computeTangentBasis();
	void computeCentroid();
	void flipFaceOrientation();
};
