
#include <Windows.h>
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>
#include "textfile.h"
#include "AuxilaryIO.h"
#include "VBO.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instance.h"
#include <string>
#include <tuple>
#include "global.h"


//models
model skybox_mod;
model chamber_mod;
model windows_mod;

//materials
material skybox_mat;
material chamber_mat;
material windows_mat;

//instances
instance skybox_inst;
instance chamber_inst;
instance windows_inst;

//demo variables
int cameraRotationDirection = 1;
float cameraRotationSpeed = 50.0f;
int cameraZoomDirection = 1;
float cameraZoomSpeed=50.0f;

bool pause = true;

float lightSpeed = 0.05f;
glm::vec3 lightMovementAmplitude(5,15,15);
float sphereRotationSpeed = 50;

void init() {
	//enable backface culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	//set depth and transparency
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//set transformations
	camera::ViewMatrix = glm::lookAt(   // View matrix
		glm::vec3(0, 0, 0), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 1), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);

	lights::light0.position = glm::vec3(15, 2, 16);
	lights::light0.BindShadowMap();
	//glEnable(GL_POLYGON_SMOOTH);
}

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	camera::ratio = 1.0* w / h;
	// Reset the coordinate system before modifying
	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	// Set the correct perspective.
	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
	viewport::width = w;
	viewport::height = h;
}



void renderScene(void) {
	time::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
	time::deltaTime = ((float)(time::currentFrameTime - time::previousFrameTime)) / 1000.0;
	time::previousFrameTime = time::currentFrameTime;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (controls::Key_A || controls::Key_D) {
		camera::ViewMatrix = glm::translate(camera::ViewMatrix,-camera::position);
		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, glm::radians(cameraRotationDirection*time::deltaTime*cameraRotationSpeed), glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);

		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, glm::radians(cameraRotationDirection*time::deltaTime*cameraRotationSpeed), glm::vec3(0, 1, 0));
	}
	if (controls::Key_L) {
		camera::position += glm::vec3(time::deltaTime, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix,glm::vec3(time::deltaTime,0,0));
	}
	if(controls::Key_J){
		camera::position += glm::vec3(-time::deltaTime, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix,glm::vec3(-time::deltaTime,0,0));
	}
	if (controls::Key_I) {
		camera::position += glm::vec3(0, 0, time::deltaTime);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, time::deltaTime));
	}
	if (controls::Key_K) {
		camera::position += glm::vec3(0, 0, -time::deltaTime);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, -time::deltaTime));
	}
	if (controls::Key_U) {
		camera::position += glm::vec3(0,time::deltaTime,0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0,time::deltaTime,0));
	}
	if (controls::Key_O) {
		camera::position += glm::vec3(0, -time::deltaTime, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, -time::deltaTime, 0));
	}
	if (controls::Key_W || controls::Key_S) {
		camera::FOV += time::deltaTime*cameraZoomDirection*cameraZoomSpeed;
		camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
	}
	static float timer = 0;
	if(!pause)
		timer += time::deltaTime;
	lights::light0.position.y = lightMovementAmplitude.y *-(cos(timer*lightSpeed*2) - 1);
	lights::light0.position.z = lightMovementAmplitude.z*cos(timer*lightSpeed);


	//render the skybox
	glDepthMask(GL_FALSE);
	skybox_inst.Render();
	glDepthMask(GL_TRUE);
	
	//render objects that cast shadows
	windows_inst.mat->SwitchToLight();
	
	lights::light0.StartFirstPass();
	glCullFace(GL_BACK);
	windows_inst.Render();
	lights::light0.EndFirstPass();

	lights::light0.StartSecondPass();
	glCullFace(GL_FRONT);
	windows_inst.Render();
	lights::light0.EndSecondPass();
	glCullFace(GL_BACK);

	//render objects that receive shadows
	lights::light0.StartThirdPass();
	chamber_inst.Render();
	lights::light0.EndThirdPass();

	//rende the rest of the object
	windows_inst.mat->SwitchToObject();
	windows_inst.Render();

	glutSwapBuffers();
}

void changeSkybox(std::string skyboxPath) {
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);

}

void processNormalKeys(unsigned char key, int x, int y) {
	std::string skyboxPath;
	std::vector<std::string> skyboxPaths;
	static float zoom = 45.0;
	switch (key)
	{
	case ' ':
		pause = !pause;
		break;
	case 27:
		exit(0);
	case 'a':
		controls::Key_A = true;
		cameraRotationDirection = -1;
		break;
	case 'd':
		controls::Key_D = true;
		cameraRotationDirection = 1;
		break;
	case 'w':
		controls::Key_W = true;
		cameraZoomDirection = 1;
		break;
	case 's':
		controls::Key_S = true;
		cameraZoomDirection = -1;
		break;
	case 'i':
		controls::Key_I = true;
		break;
	case 'j':
		controls::Key_J = true;
		break;
	case 'k':
		controls::Key_K = true;
		break;
	case 'l':
		controls::Key_L = true;
		break;
	case 'o':
		controls::Key_O = true;
		break;
	case 'u':
		controls::Key_U = true;
		break;
	case '0':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox0/";
		changeSkybox(skyboxPath);
		break;
	case '1':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox1/";
		changeSkybox(skyboxPath);

		break;
	case '2':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox2/";
		changeSkybox(skyboxPath);
		break;
	case '3':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox3/";
		changeSkybox(skyboxPath);
		break;
	case '4':
		skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox4/";
		changeSkybox(skyboxPath);
		break;
	default:
		break;
	}
	glutPostRedisplay();
}


void ProcessNormalKeysUp(unsigned char key, int x, int y) {
	switch (key) {
	case 'a':
		controls::Key_A = false;
		break;
	case 'd':
		controls::Key_D = false;
		break;
	case 'w':
		controls::Key_W = false;
		break;
	case 's':
		controls::Key_S = false;
		break;
	case 'i':
		controls::Key_I = false;
		break;
	case 'j':
		controls::Key_J = false;
		break;
	case 'k':
		controls::Key_K = false;
		break;
	case 'l':
		controls::Key_L = false;
		break;
	case 'o':
		controls::Key_O = false;
		break;
	case 'u' :
		controls::Key_U = false;
		break;
	}

}

void processSpecialKeys(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = true;
		chamber_mat.choice++;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = true;
		chamber_mat.choice--;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void processSpecialKeysUp(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = false;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = false;
		break;
	default:
		break;
	}
}


void loadShaders() {
	//create a list of the shader paths
	std::vector<std::tuple<std::string, std::string>> shaderPaths;
	std::vector<GLuint> shaderIDs;
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DebugShader/debug.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DebugShader/debug.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/SkyboxShader/skybox.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/SkyboxShader/skybox.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/NormalMapShader/normalMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/NormalMapShader/normalMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelShader/fresnel.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelShader/fresnel.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureShader/texture.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureShader/texture.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ReflectionShader/reflection.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ReflectionShader/reflection.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/RefractionShader/refraction.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/RefractionShader/refraction.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DepthMapShader/depth.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DepthMapShader/depth.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.frag"));

	//load the shaders
	shaders::loadShaders(shaderPaths, shaderIDs);
	//assign to global variables
	skybox_mat.ShaderID = shaderIDs[1];

	windows_mat.objectShaderID = shaderIDs[5];
	windows_mat.AlphaValue = 0.75f;
	windows_mat.lightShaderID = shaderIDs[14];
	windows_mat.SwitchToObject();

	chamber_mat.objectShaderID = shaderIDs[13];
	chamber_mat.lightShaderID = shaderIDs[14];
	chamber_mat.SwitchToObject();

	lights::light0.depthMapShaderID = shaderIDs[11];
}

void loadTexture() {
	//load the skybox maps
	std::string skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/skybox1/";
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);

	chamber_mat.DiffuseMapID = textures::loadTextureDDS("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/stonewall.png");
	chamber_mat.NormalMapID = textures::loadTextureDDS("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/stonewall_n.png");
	windows_mat.DiffuseMapID = textures::loadTextureDDS("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/zeldaGlass.jpg");
	windows_mat.NormalMapID = textures::loadTextureDDS("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/textures/zeldaGlass_n.png");
	windows_mat.AlphaValue = 0.44;
}

void loadObject() {
	//load the skybox cube
	if (!loadOBJ("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/models/skybox.obj", skybox_mod.vertices, skybox_mod.normals, skybox_mod.UVs))
		exit(-1);
	skybox_mod.flipFaceOrientation();
	buffers::Load(skybox_mod);
	std::cout << "Loadign" << std::endl;
	if (!loadOBJ("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/models/demo_room.obj", chamber_mod.vertices, chamber_mod.normals, chamber_mod.UVs))
		exit(-1);
	chamber_mod.computeTangentBasis();
	buffers::Load(chamber_mod);
	if (!loadOBJ("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGLTemplate/OpenGLTemplate/models/demo_windows.obj", windows_mod.vertices, windows_mod.normals, windows_mod.UVs))
		exit(-1);
	windows_mod.computeTangentBasis();
	buffers::Load(windows_mod);
}



int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1600, 1200);
	glutCreateWindow("GLSL Example");

	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(ProcessNormalKeysUp);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(processSpecialKeysUp);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glewInit();
	init();

	if (glewIsSupported("GL_VERSION_4_5"))
		printf("Ready for OpenGL 4.5\n");
	else {
		printf("OpenGL 4.5 not supported\n");
		//exit(1);
	}
	//load the auxilary data to memory and pass them to GPU
	std::cout << "loading shaders" << std::endl;
	loadShaders();
	loadObject();
	loadTexture();
	//connect the materials and models to instances
	///skybox
	skybox_inst.mod = &skybox_mod;
	skybox_inst.mat = &skybox_mat;

	chamber_inst.mod = &chamber_mod;
	chamber_inst.mat = &chamber_mat;

	windows_inst.mod = &windows_mod;
	windows_inst.mat = &windows_mat;

	camera::ViewMatrix = glm::rotate(camera::ViewMatrix, 79.0f, glm::vec3(0, 1, 0));
	glutMainLoop();

	return 0;
}

