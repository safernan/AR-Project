#pragma once
#include <string>
#include "model.h"
#include "material.h"
struct instance {
	std::string name;
	model* mod=NULL;
	material * mat = NULL;
	glm::mat4 transform = glm::mat4(1.0);
	glm::mat4 rotate(GLfloat rotX,GLfloat rotY,GLfloat rotZ);
	glm::mat4 scale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ);
	glm::mat4 rotateAround(GLfloat rotX, GLfloat rotY, GLfloat rotZ,glm::vec3 pivot);
	glm::mat4 translate(GLfloat transX, GLfloat transY, GLfloat transZ);
	void BindMaterial();
	void BindModel();
	void Render();
	void RenderWithoutMaterials();
};
