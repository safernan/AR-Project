#include "material.h"
#include "global.h"

void material::BindTextures()
{
	int mapCounter = 0;
	if (ShaderID == -1)return;
	glUseProgram(ShaderID);
	if (DiffuseMapID != -1) {
		glActiveTexture(GL_TEXTURE0+mapCounter);
		glBindTexture(GL_TEXTURE_2D, DiffuseMapID);

		GLuint DiffuseMapLocation = glGetUniformLocation(ShaderID, "diffuseMap");
		glUniform1i(DiffuseMapLocation, mapCounter++);
	}
	if (NormalMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, NormalMapID);
		GLuint NormalMapLocation = glGetUniformLocation(ShaderID, "normalMap");
		glUniform1i(NormalMapLocation, mapCounter++);
	}
	if (SpecularMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, SpecularMapID);
		GLuint SpecularMapLocation = glGetUniformLocation(ShaderID, "specularMap");
		glUniform1i(SpecularMapLocation, mapCounter++);
	}

	if (SkyboxMapID != -1) {
		glActiveTexture(GL_TEXTURE0+mapCounter);
		glBindTexture(GL_TEXTURE_CUBE_MAP, SkyboxMapID);
		GLuint SkyboxMapLocation = glGetUniformLocation(ShaderID,"skyboxMap");
		glUniform1i(SkyboxMapLocation, mapCounter++);
	}
}

void material::BindValues()
{
	GLuint AlphaValueLocation = glGetUniformLocation(ShaderID, "alphaValue");
	glUniform1f(AlphaValueLocation, AlphaValue);

	GLuint LPLocation = glGetUniformLocation(ShaderID, "LP");
	glUniform3f(LPLocation, lights::light0.position.x, lights::light0.position.y, lights::light0.position.z);

	GLuint viewPosLocation = glGetUniformLocation(ShaderID, "viewPos");
	glUniform3f(viewPosLocation, camera::position.x, camera::position.y, camera::position.z);

	GLuint choiceLocation = glGetUniformLocation(ShaderID, "choice");
	glUniform1i(choiceLocation, choice);

}