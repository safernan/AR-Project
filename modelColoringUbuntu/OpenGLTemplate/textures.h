#pragma once
#include<GL/freeglut.h>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

namespace textures {
	GLuint loadCubemap(std::vector<std::string> face);
	GLuint loadTexture(std::string path);
	GLuint loadTexture(cv::Mat mat,GLuint textureID=-1);
};
