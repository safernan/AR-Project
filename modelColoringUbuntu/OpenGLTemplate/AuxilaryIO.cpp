#include "AuxilaryIO.h"
#include "debug.h"
//stl
#include <vector>
#include <iostream>
//glm
#include <glm/glm.hpp>
//assimp
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>     // Post processing flags
#include <assimp/scene.h>

bool loadOBJ(const char * path, std::vector <glm::vec3> & out_vertices, std::vector <glm::vec3> & out_normals, std::vector <glm::vec2> & out_uvs)
{
	// Create an instance of the Importer class

	Assimp::Importer importer;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll 
	// propably to request more postprocessing than we do in this example.
	const aiScene* scene = importer.ReadFile(path,
		aiProcess_Triangulate
		//|aiProcess_JoinIdenticalVertices
	);

	if (!scene)
	{

		std::string errorString = importer.GetErrorString();
		//printConsole(errorString);
		return false;
	}
	else {
		size_t size_acc = 0;
		for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
			size_acc += scene->mMeshes[j]->mNumVertices;
		}
		out_vertices.reserve(size_acc);
		out_normals.reserve(size_acc);
		out_uvs.reserve(size_acc);
		for (unsigned int j = 0; j < scene->mNumMeshes; j++) {
			aiMesh* mesh = scene->mMeshes[j];
			for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[0]].x, mesh->mVertices[mesh->mFaces[i].mIndices[0]].y, mesh->mVertices[mesh->mFaces[i].mIndices[0]].z));
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[1]].x, mesh->mVertices[mesh->mFaces[i].mIndices[1]].y, mesh->mVertices[mesh->mFaces[i].mIndices[1]].z));
				out_vertices.push_back(glm::vec3(mesh->mVertices[mesh->mFaces[i].mIndices[2]].x, mesh->mVertices[mesh->mFaces[i].mIndices[2]].y, mesh->mVertices[mesh->mFaces[i].mIndices[2]].z));

				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[0]].x, mesh->mNormals[mesh->mFaces[i].mIndices[0]].y, mesh->mNormals[mesh->mFaces[i].mIndices[0]].z));
				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[1]].x, mesh->mNormals[mesh->mFaces[i].mIndices[1]].y, mesh->mNormals[mesh->mFaces[i].mIndices[1]].z));
				out_normals.push_back(glm::vec3(mesh->mNormals[mesh->mFaces[i].mIndices[2]].x, mesh->mNormals[mesh->mFaces[i].mIndices[2]].y, mesh->mNormals[mesh->mFaces[i].mIndices[2]].z));

				out_uvs.push_back(glm::vec2(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].y/*, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[0]].z*/));
				out_uvs.push_back(glm::vec2(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].y/*, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[1]].z*/));
				out_uvs.push_back(glm::vec2(mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].x, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].y/*, mesh->mTextureCoords[0][mesh->mFaces[i].mIndices[2]].z*/));

			}
		}
	}
	return true;
}
