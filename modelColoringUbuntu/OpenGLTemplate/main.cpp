
#include <windows.h>
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <string>
#include <tuple>

#include <gl/glew.h>
#include <gl/glut.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <ar/config.h>
#include <ar/video.h>
#include <ar/param.h>			// arparamdisp()
#include <ar/ar.h>
#include <ar/gsub_lite.h>

#include "textfile.h"
#include "auxilaryio.h"
#include "vbo.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instance.h"

#include "global.h"
#include "collisiondetection.h"


//models
model skybox_mod;
model sculpt_mod;
model finger_mod;

//materials
material skybox_mat;
material sculpt_mat;
material finger_mat;
//instances
instance skybox_inst;
instance sculpt_inst;
instance finger_inst;

ARdouble m[16];
//
////OpenCV
cv::Mat textureMask;
//cv::Mat brushMask;
////demo variables
//int cameraRotationDirection = 1;
//float cameraRotationSpeed = 50.0f;
//int cameraZoomDirection = 1;
//float cameraZoomSpeed=50.0f;
//
//bool pause = true;
//
//void init() {
//	//enable backface culling
//	glFrontFace(GL_CCW);
//	glCullFace(GL_BACK);
//	glEnable(GL_CULL_FACE);
//	//set depth and transparency
//	glEnable(GL_DEPTH_TEST);
//	glClearColor(1.0, 1.0, 1.0, 0.0);
//	glEnable(GL_BLEND);
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	//set transformations
//	
//	camera::ViewMatrix = glm::lookAt(   // View matrix
//		glm::vec3(0, 0, 0), // Camera is at (4,3,3), in World Space
//		glm::vec3(0, 0, 1), // and looks at the origin
//		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
//	);
//	//openCV
//	brushMask = cv::imread("D:/ARProject/resources/brushes/gauss.jpg");
//	cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);// Create a window for display.
//	//cvWaitKey(0);
//	textureMask = cv::imread("D:/ARProject/resources/textures/testUV2.png");
//	cv::imshow("Display window", textureMask);
//}
//
//void changeSize(int w, int h) {
//
//	// Prevent a divide by zero, when window is too short
//	// (you cant make a window of zero width).
//	if (h == 0)
//		h = 1;
//	camera::ratio = 1.0* w / h;
//	// Reset the coordinate system before modifying
//	// Set the viewport to be the entire window
//	glViewport(0, 0, w, h);
//	// Set the correct perspective.
//	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
//	viewport::width = w;
//	viewport::height = h;
//}
//
//
//
//void handleInput() {
//	if (controls::Key_A || controls::Key_D) {
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, -camera::position);
//		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, glm::radians(cameraRotationDirection*dtime::deltaTime*cameraRotationSpeed), glm::vec3(0, 1, 0));
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);
//
//		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, glm::radians(cameraRotationDirection*dtime::deltaTime*cameraRotationSpeed), glm::vec3(0, 1, 0));
//	}
//	if (controls::Key_L) {
//		camera::position += glm::vec3(dtime::deltaTime, 0, 0);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(dtime::deltaTime, 0, 0));
//	}
//	if (controls::Key_J) {
//		camera::position += glm::vec3(-dtime::deltaTime, 0, 0);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(-dtime::deltaTime, 0, 0));
//	}
//	if (controls::Key_I) {
//		camera::position += glm::vec3(0, 0, dtime::deltaTime);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, dtime::deltaTime));
//	}
//	if (controls::Key_K) {
//		camera::position += glm::vec3(0, 0, -dtime::deltaTime);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, -dtime::deltaTime));
//	}
//	if (controls::Key_U) {
//		camera::position += glm::vec3(0, dtime::deltaTime, 0);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, dtime::deltaTime, 0));
//	}
//	if (controls::Key_O) {
//		camera::position += glm::vec3(0, -dtime::deltaTime, 0);
//		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, -dtime::deltaTime, 0));
//	}
//	if (controls::Key_W || controls::Key_S) {
//		camera::FOV += dtime::deltaTime*cameraZoomDirection*cameraZoomSpeed;
//		camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
//	}
//	if (controls::Key_Up) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(0,dtime::deltaTime,0))*finger_inst.transform;
//	}
//	if (controls::Key_Down) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(0, -dtime::deltaTime, 0))*finger_inst.transform;
//	}
//	if (controls::Key_Left) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(-dtime::deltaTime,0, 0))*finger_inst.transform;
//	}
//	if (controls::Key_Right) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(dtime::deltaTime,0, 0))*finger_inst.transform;
//	}
//	if (controls::Key_2) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, dtime::deltaTime))*finger_inst.transform;
//	}
//	if (controls::Key_8) {
//		finger_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, -dtime::deltaTime))*finger_inst.transform;
//	}
//}
//
//glm::vec3 barycentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p) {
//	float w1 = glm::length(glm::cross(p3 - p, p2 - p));
//	float w2 = glm::length(glm::cross(p3 - p, p1 - p));
//	float w3 = glm::length(glm::cross(p2 - p, p1 - p));
//	float sum = glm::length(glm::cross(p3 - p1, p2 - p1));
//	return glm::vec3(w1 / sum, w2 / sum, w3 / sum);
//}
//
//void handlePhysics() {
//	glm::vec3 scale;
//	glm::quat rotation;
//	glm::vec3 translation;
//	glm::vec3 skew;
//	glm::vec4 perspective;
//	glm::decompose(finger_inst.transform, scale, rotation, translation, skew, perspective);
//	//check if the finger collides with the model
//	contact c;
//	if (collision::point2model(translation, *sculpt_inst.mod, &c)) {
//		std::cout << "contact:" << glm::to_string(c.contactPoint) << std::endl;
//		c.mod->actives[c.triangle.x] = 1.0f;
//		c.mod->actives[c.triangle.y] = 1.0f;
//		c.mod->actives[c.triangle.z] = 1.0f;
//		//print uv coords of triangle
//		std::cout << "uvs are " << glm::to_string(c.mod->UVs[c.triangle.x]) << std::endl;
//		std::cout << "uvs are " << glm::to_string(c.mod->UVs[c.triangle.y]) << std::endl;
//		std::cout << "uvs are " << glm::to_string(c.mod->UVs[c.triangle.z]) << std::endl;
//		//interpolate the uv coords to find the values for the contact point
//		glm::vec3 weights = barycentric(c.mod->vertices[c.triangle.x], c.mod->vertices[c.triangle.y], c.mod->vertices[c.triangle.z],c.contactPoint);
//		glm::vec2 uvs = c.mod->UVs[c.triangle.x] * weights.x + c.mod->UVs[c.triangle.y] * weights.y + c.mod->UVs[c.triangle.z] * weights.z;
//		int TEXT_SIZE = textureMask.cols;
//		int kernel_size = brushMask.cols;
//		for (int i = 0; i < kernel_size; i++) {
//			for (int j = 0; j < kernel_size; j++) {
//				textureMask.at<cv::Vec3b>((TEXT_SIZE - 1) * uvs.y+i, (TEXT_SIZE - 1) * uvs.x+j) =
//					textureMask.at<cv::Vec3b>((TEXT_SIZE - 1) * uvs.y + i, (TEXT_SIZE - 1) * uvs.x + j)*(1.0-brushMask.at<cv::Vec3b>(i, j)[0] / 255.0f)+
//					cv::Vec3b(255, 0, 0)*(brushMask.at<cv::Vec3b>(i,j)[0]/255.0f);
//			}
//		}
//
//		sculpt_mat.DiffuseMapID = textures::loadTexture(textureMask,sculpt_mat.DiffuseMapID);
//		cv::imshow("Display window", textureMask);
//	}
//
//}
//
//void renderScene(void) {
//	//update the delta time
//	dtime::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
//	dtime::deltaTime = ((float)(dtime::currentFrameTime - dtime::previousFrameTime)) / 1000.0;
//	dtime::previousFrameTime = dtime::currentFrameTime;
//
//	//update with input
//	handleInput();
//	handlePhysics();
//	//render the scene
//	///clear the buffer
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	///render the skybox
//	glDepthMask(GL_FALSE);
//	skybox_inst.Render();
//	glDepthMask(GL_TRUE);
//	///render everything else
//	sculpt_inst.Render();
//	finger_inst.Render();
//	///swap buffers
//	glutSwapBuffers();
//}
//
//void changeSkybox(std::string skyboxPath) {
//	std::vector<std::string> skyboxPaths;
//	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
//	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
//	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
//	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
//	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
//	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
//	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
//
//}
//
//void processNormalKeys(unsigned char key, int x, int y) {
//	std::string skyboxPath;
//	std::vector<std::string> skyboxPaths;
//	static float zoom = 45.0;
//	switch (key)
//	{
//	case ' ':
//		pause = !pause;
//		break;
//	case 27:
//		exit(0);
//	case 'a':
//		controls::Key_A = true;
//		cameraRotationDirection = -1;
//		break;
//	case 'd':
//		controls::Key_D = true;
//		cameraRotationDirection = 1;
//		break;
//	case 'w':
//		controls::Key_W = true;
//		cameraZoomDirection = 1;
//		break;
//	case 's':
//		controls::Key_S = true;
//		cameraZoomDirection = -1;
//		break;
//	case 'i':
//		controls::Key_I = true;
//		break;
//	case 'j':
//		controls::Key_J = true;
//		break;
//	case 'k':
//		controls::Key_K = true;
//		break;
//	case 'l':
//		controls::Key_L = true;
//		break;
//	case 'o':
//		controls::Key_O = true;
//		break;
//	case 'u':
//		controls::Key_U = true;
//		break;
//	case '0':
//		break;
//	case '1':
//		break;
//	case '2':
//		controls::Key_2 = true;
//		break;
//	case '8':
//		controls::Key_8 = true;
//		break;
//	default:
//		break;
//	}
//	glutPostRedisplay();
//}
//
//
//void ProcessNormalKeysUp(unsigned char key, int x, int y) {
//	switch (key) {
//	case 'a':
//		controls::Key_A = false;
//		break;
//	case 'd':
//		controls::Key_D = false;
//		break;
//	case 'w':
//		controls::Key_W = false;
//		break;
//	case 's':
//		controls::Key_S = false;
//		break;
//	case 'i':
//		controls::Key_I = false;
//		break;
//	case 'j':
//		controls::Key_J = false;
//		break;
//	case 'k':
//		controls::Key_K = false;
//		break;
//	case 'l':
//		controls::Key_L = false;
//		break;
//	case 'o':
//		controls::Key_O = false;
//		break;
//	case 'u' :
//		controls::Key_U = false;
//		break;
//	case '2':
//		controls::Key_2 = false;
//		break;
//	case '8':
//		controls::Key_8 = false;
//		break;
//	}
//
//}
//
//void processSpecialKeys(int key, int x, int y) {
//	switch (key)
//	{
//	case GLUT_KEY_RIGHT:
//		controls::Key_Right = true;
//		break;
//	case GLUT_KEY_LEFT:
//		controls::Key_Left = true;
//		break;
//	case GLUT_KEY_UP:
//		controls::Key_Up = true;
//		break;
//	case GLUT_KEY_DOWN:
//		controls::Key_Down = true;
//		break;
//	default:
//		break;
//	}
//	glutPostRedisplay();
//}
//
//void processSpecialKeysUp(int key, int x, int y) {
//	switch (key)
//	{
//	case GLUT_KEY_RIGHT:
//		controls::Key_Right = false;
//		break;
//	case GLUT_KEY_LEFT:
//		controls::Key_Left = false;
//		break;
//	case GLUT_KEY_UP:
//		controls::Key_Up = false;
//		break;
//	case GLUT_KEY_DOWN:
//		controls::Key_Down = false;
//		break;
//	default:
//		break;
//	}
//}
//
//
void loadShaders() {
	//create a list of the shader paths
	std::vector<std::tuple<std::string, std::string>> shaderPaths;
	std::vector<GLuint> shaderIDs;
	shaderPaths.push_back(std::make_tuple(
		"D:/ARProject/modelColoring/OpenGLTemplate/shaders/PalleteShader/pallete.vert",
		"D:/ARProject/modelColoring/OpenGLTemplate/shaders/PalleteShader/pallete.frag"));

	//load the shaders
	shaders::loadShaders(shaderPaths, shaderIDs);
	//assign to global variables
	//skybox_mat.ShaderID = shaderIDs[1];
	sculpt_mat.ShaderID = shaderIDs[0];
	//finger_mat.ShaderID = shaderIDs[0];
}

void loadTexture() {
	//sculpt_mat.DiffuseMapID = textures::loadTexture("D:/ARProject/modelColoring/OpenGLTemplate/textures/stonewall.png");
	textureMask = cv::imread("D:/ARProject/resources/textures/testUV2.png");
	sculpt_mat.DiffuseMapID = textures::loadTexture(textureMask);
	//sculpt_mat.NormalMapID = textures::loadTexture("D:/ARProject/modelColoring/OpenGLTemplate/textures/stonewall_n.png");
}
//
void loadObject() {
	//load the sculpture
	if (!loadOBJ("D:/ARProject/resources/models/UVSphere.obj", sculpt_mod.vertices,sculpt_mod.normals,sculpt_mod.UVs))
		exit(-1);
	sculpt_mod.computeTangentBasis();
	buffers::Load(sculpt_mod);
}
//
//
//
//int main(int argc, char **argv) {
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//	glutInitWindowPosition(0, 0);
//	glutInitWindowSize(1600, 1200);
//	glutCreateWindow("GLSL Example");
//
//	glutDisplayFunc(renderScene);
//	glutIdleFunc(renderScene);
//	glutReshapeFunc(changeSize);
//	glutKeyboardFunc(processNormalKeys);
//	glutKeyboardUpFunc(ProcessNormalKeysUp);
//	glutSpecialFunc(processSpecialKeys);
//	glutSpecialUpFunc(processSpecialKeysUp);
//
//	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
//	glewInit();
//	init();
//
//	if (glewIsSupported("GL_VERSION_4_5"))
//		printf("Ready for OpenGL 4.5\n");
//	else {
//		printf("OpenGL 4.5 not supported\n");
//		//exit(1);
//	}
//	//load the auxilary data to memory and pass them to GPU
//	std::cout << "loading shaders" << std::endl;
//	loadShaders();
//	loadObject();
//	loadTexture();
//	//connect the materials and models to instances
//	///skybox
//	skybox_inst.mod = &skybox_mod;
//	skybox_inst.mat = &skybox_mat;
//	///sculpture
//	sculpt_inst.mod = &sculpt_mod;
//	sculpt_inst.mat = &sculpt_mat;
//	///finger
//	finger_inst.mod = &finger_mod;
//	finger_inst.mat = &finger_mat;
//	finger_inst.scale(0.1, 0.1, 0.1);
//	camera::ViewMatrix = glm::rotate(camera::ViewMatrix, 79.0f, glm::vec3(0, 1, 0));
//	glutMainLoop();
//
//	return 0;
//}
//
/*
*  simpleLite.c
*
*  Some code to demonstrate use of ARToolKit.
*
*  Press '?' while running for help on available key commands.
*
*  Disclaimer: IMPORTANT:  This Daqri software is supplied to you by Daqri
*  LLC ("Daqri") in consideration of your agreement to the following
*  terms, and your use, installation, modification or redistribution of
*  this Daqri software constitutes acceptance of these terms.  If you do
*  not agree with these terms, please do not use, install, modify or
*  redistribute this Daqri software.
*
*  In consideration of your agreement to abide by the following terms, and
*  subject to these terms, Daqri grants you a personal, non-exclusive
*  license, under Daqri's copyrights in this original Daqri software (the
*  "Daqri Software"), to use, reproduce, modify and redistribute the Daqri
*  Software, with or without modifications, in source and/or binary forms;
*  provided that if you redistribute the Daqri Software in its entirety and
*  without modifications, you must retain this notice and the following
*  text and disclaimers in all such redistributions of the Daqri Software.
*  Neither the name, trademarks, service marks or logos of Daqri LLC may
*  be used to endorse or promote products derived from the Daqri Software
*  without specific prior written permission from Daqri.  Except as
*  expressly stated in this notice, no other rights or licenses, express or
*  implied, are granted by Daqri herein, including but not limited to any
*  patent rights that may be infringed by your derivative works or by other
*  works in which the Daqri Software may be incorporated.
*
*  The Daqri Software is provided by Daqri on an "AS IS" basis.  DAQRI
*  MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
*  THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE, REGARDING THE DAQRI SOFTWARE OR ITS USE AND
*  OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
*
*  IN NO EVENT SHALL DAQRI BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
*  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*  INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
*  MODIFICATION AND/OR DISTRIBUTION OF THE DAQRI SOFTWARE, HOWEVER CAUSED
*  AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
*  STRICT LIABILITY OR OTHERWISE, EVEN IF DAQRI HAS BEEN ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*
*  Copyright 2015 Daqri LLC. All Rights Reserved.
*  Copyright 2002-2015 ARToolworks, Inc. All Rights Reserved.
*
*  Author(s): Philip Lamb.
*
*/

// ============================================================================
//	Includes
// ============================================================================

#include <stdio.h>
#include <string.h>
#ifdef _WIN32
#  define snprintf _snprintf
#endif
#include <stdlib.h>					// malloc(), free()
#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif
#include <AR/config.h>
#include <AR/video.h>
#include <AR/param.h>			// arParamDisp()
#include <AR/ar.h>
#include <AR/gsub_lite.h>


// ============================================================================
//	Constants
// ============================================================================

#define VIEW_SCALEFACTOR		1.0         // Units received from ARToolKit tracking will be multiplied by this factor before being used in OpenGL drawing.
#define VIEW_DISTANCE_MIN		40.0        // Objects closer to the camera than this will not be displayed. OpenGL units.
#define VIEW_DISTANCE_MAX		10000.0     // Objects further away from the camera than this will not be displayed. OpenGL units.

// ============================================================================
//	Global variables
// ============================================================================

// Preferences.
static int windowed = TRUE;                     // Use windowed (TRUE) or fullscreen mode (FALSE) on launch.
static int windowWidth = 640;					// Initial window width, also updated during program execution.
static int windowHeight = 480;                  // Initial window height, also updated during program execution.
static int windowDepth = 32;					// Fullscreen mode bit depth.
static int windowRefresh = 0;					// Fullscreen mode refresh rate. Set to 0 to use default rate.

												// Image acquisition.
static ARUint8		*gARTImage = NULL;
static int          gARTImageSavePlease = FALSE;

// Marker detection.
static ARHandle		*gARHandle = NULL;
static ARPattHandle	*gARPattHandle = NULL;
static long			gCallCountMarkerDetect = 0;

// Transformation matrix retrieval.
static AR3DHandle	*gAR3DHandle = NULL;
static ARdouble		gPatt_width = 80.0;	// Per-marker, but we are using only 1 marker.
static ARdouble		gPatt_trans[3][4];		// Per-marker, but we are using only 1 marker.
static int			gPatt_found = FALSE;	// Per-marker, but we are using only 1 marker.
static int			gPatt_id;				// Per-marker, but we are using only 1 marker.

											// Drawing.
static ARParamLT *gCparamLT = NULL;
static ARGL_CONTEXT_SETTINGS_REF gArglSettings = NULL;
static int gShowHelp = 1;
static int gShowMode = 1;
static int gDrawRotate = FALSE;
static float gDrawRotateAngle = 0;			// For use in drawing.


											// ============================================================================
											//	Function prototypes.
											// ============================================================================

static void print(const char *text, const float x, const float y, int calculateXFromRightEdge, int calculateYFromTopEdge);
static void drawBackground(const float width, const float height, const float x, const float y);
static void printHelpKeys();
static void printMode();

// ============================================================================
//	Functions
// ============================================================================

// Something to look at, draw a rotating colour cube.
static void DrawCube(void)
{
	// Colour cube data.
	 
	//int i;
	float fSize = 40.0f;
	//
	//const GLfloat cube_vertices[8][3] = {
	//	/* +z */{ 0.5f, 0.5f, 0.5f },{ 0.5f, -0.5f, 0.5f },{ -0.5f, -0.5f, 0.5f },{ -0.5f, 0.5f, 0.5f },
	//	/* -z */{ 0.5f, 0.5f, -0.5f },{ 0.5f, -0.5f, -0.5f },{ -0.5f, -0.5f, -0.5f },{ -0.5f, 0.5f, -0.5f } };
	//const GLubyte cube_vertex_colors[8][4] = {
	//	{ 255, 255, 255, 255 },{ 255, 255, 0, 255 },{ 0, 255, 0, 255 },{ 0, 255, 255, 255 },
	//	{ 255, 0, 255, 255 },{ 255, 0, 0, 255 },{ 0, 0, 0, 255 },{ 0, 0, 255, 255 } };
	//const GLubyte cube_faces[6][4] = { /* ccw-winding */
	//	/* +z */{ 3, 2, 1, 0 }, /* -y */{ 2, 3, 7, 6 }, /* +y */{ 0, 1, 5, 4 },
	//	/* -x */{ 3, 0, 4, 7 }, /* +x */{ 1, 2, 6, 5 }, /* -z */{ 4, 5, 6, 7 } };

	//int sculpSize = sculpt_mod.vertices.size();
	//

	instance inst;
	inst.mod = &sculpt_mod;
	inst.mat = &sculpt_mat;
	
	glm::mat4 matrix = glm::mat4(m[0], m[4], m[8], m[12],
		m[1], m[5], m[9], m[13],
		m[2], m[6], m[10], m[14],
		m[3], m[7], m[11], m[15]
	);


	inst.transform = glm::mat4(1.0f);
	inst.Render();
	//glPushMatrix(); // Save world coordinate system.
	//glRotatef(gDrawRotateAngle, 0.0f, 0.0f, 1.0f); // Rotate about z axis.
	//glScalef(fSize, fSize, fSize);
	//glTranslatef(0.0f, 0.0f, 0.5f); // Place base of cube on marker surface.
	//glDisable(GL_LIGHTING);
	//glDisable(GL_TEXTURE_2D);
	//glDisable(GL_BLEND);
	//glColorPointer(4, GL_UNSIGNED_BYTE, 0, cube_vertex_colors);
	//glVertexPointer(3, GL_FLOAT, 0, cube_vertices);
	//glEnableClientState(GL_VERTEX_ARRAY);
	//glEnableClientState(GL_COLOR_ARRAY);
	//inst.Render();
	///*for (i = 0; i < 6; i++) {
	//	glDrawElements(GL_TRIANGLE_FAN, 4, GL_UNSIGNED_BYTE, &(cube_faces[i][0]));
	//}*/
	//glDisableClientState(GL_COLOR_ARRAY);
	//glColor4ub(0, 0, 0, 255);
	//for (i = 0; i < 6; i++) {
	//	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, &(cube_faces[i][0]));
	//}
	//glPopMatrix();    // Restore world coordinate system.
}

static void DrawCubeUpdate(float timeDelta)
{
	if (gDrawRotate) {
		gDrawRotateAngle += timeDelta * 45.0f; // Rotate cube at 45 degrees per second.
		if (gDrawRotateAngle > 360.0f) gDrawRotateAngle -= 360.0f;
	}
}

static int setupCamera(const char *cparam_name, char *vconf, ARParamLT **cparamLT_p, ARHandle **arhandle, AR3DHandle **ar3dhandle)
{
	ARParam			cparam;
	int				xsize, ysize;
	AR_PIXEL_FORMAT pixFormat;

	// Open the video path.
	if (arVideoOpen(vconf) < 0) {
		ARLOGe("setupCamera(): Unable to open connection to camera.\n");
		return (FALSE);
	}

	// Find the size of the window.
	if (arVideoGetSize(&xsize, &ysize) < 0) {
		ARLOGe("setupCamera(): Unable to determine camera frame size.\n");
		arVideoClose();
		return (FALSE);
	}
	ARLOGi("Camera image size (x,y) = (%d,%d)\n", xsize, ysize);

	// Get the format in which the camera is returning pixels.
	pixFormat = arVideoGetPixelFormat();
	if (pixFormat == AR_PIXEL_FORMAT_INVALID) {
		ARLOGe("setupCamera(): Camera is using unsupported pixel format.\n");
		arVideoClose();
		return (FALSE);
	}

	// Load the camera parameters, resize for the window and init.
	if (arParamLoad(cparam_name, 1, &cparam) < 0) {
		ARLOGe("setupCamera(): Error loading parameter file %s for camera.\n", cparam_name);
		arVideoClose();
		return (FALSE);
	}
	if (cparam.xsize != xsize || cparam.ysize != ysize) {
		ARLOGw("*** Camera Parameter resized from %d, %d. ***\n", cparam.xsize, cparam.ysize);
		arParamChangeSize(&cparam, xsize, ysize, &cparam);
	}
#ifdef DEBUG
	ARLOG("*** Camera Parameter ***\n");
	arParamDisp(&cparam);
#endif
	if ((*cparamLT_p = arParamLTCreate(&cparam, AR_PARAM_LT_DEFAULT_OFFSET)) == NULL) {
		ARLOGe("setupCamera(): Error: arParamLTCreate.\n");
		return (FALSE);
	}

	if ((*arhandle = arCreateHandle(*cparamLT_p)) == NULL) {
		ARLOGe("setupCamera(): Error: arCreateHandle.\n");
		return (FALSE);
	}
	if (arSetPixelFormat(*arhandle, pixFormat) < 0) {
		ARLOGe("setupCamera(): Error: arSetPixelFormat.\n");
		return (FALSE);
	}
	if (arSetDebugMode(*arhandle, AR_DEBUG_DISABLE) < 0) {
		ARLOGe("setupCamera(): Error: arSetDebugMode.\n");
		return (FALSE);
	}
	if ((*ar3dhandle = ar3DCreateHandle(&cparam)) == NULL) {
		ARLOGe("setupCamera(): Error: ar3DCreateHandle.\n");
		return (FALSE);
	}

	if (arVideoCapStart() != 0) {
		ARLOGe("setupCamera(): Unable to begin camera data capture.\n");
		return (FALSE);
	}

	return (TRUE);
}

static int setupMarker(const char *patt_name, int *patt_id, ARHandle *arhandle, ARPattHandle **pattHandle_p)
{
	if ((*pattHandle_p = arPattCreateHandle()) == NULL) {
		ARLOGe("setupMarker(): Error: arPattCreateHandle.\n");
		return (FALSE);
	}

	// Loading only 1 pattern in this example.
	if ((*patt_id = arPattLoad(*pattHandle_p, patt_name)) < 0) {
		ARLOGe("setupMarker(): Error loading pattern file %s.\n", patt_name);
		arPattDeleteHandle(*pattHandle_p);
		return (FALSE);
	}

	arPattAttach(arhandle, *pattHandle_p);

	return (TRUE);
}

static void cleanup(void)
{
	arglCleanup(gArglSettings);
	gArglSettings = NULL;
	arPattDetach(gARHandle);
	arPattDeleteHandle(gARPattHandle);
	arVideoCapStop();
	ar3DDeleteHandle(&gAR3DHandle);
	arDeleteHandle(gARHandle);
	arParamLTFree(&gCparamLT);
	arVideoClose();
}

static void Keyboard(unsigned char key, int x, int y)
{
	int mode, threshChange = 0;
	AR_LABELING_THRESH_MODE modea;

	switch (key) {
	case 0x1B:						// Quit.
	case 'Q':
	case 'q':
		cleanup();
		exit(0);
		break;
	case ' ':
		gDrawRotate = !gDrawRotate;
		break;
	case 'X':
	case 'x':
		arGetImageProcMode(gARHandle, &mode);
		switch (mode) {
		case AR_IMAGE_PROC_FRAME_IMAGE:  mode = AR_IMAGE_PROC_FIELD_IMAGE; break;
		case AR_IMAGE_PROC_FIELD_IMAGE:
		default: mode = AR_IMAGE_PROC_FRAME_IMAGE; break;
		}
		arSetImageProcMode(gARHandle, mode);
		break;
	case 'C':
	case 'c':
		ARLOGe("*** Camera - %f (frame/sec)\n", (double)gCallCountMarkerDetect / arUtilTimer());
		gCallCountMarkerDetect = 0;
		arUtilTimerReset();
		break;
	case 'a':
	case 'A':
		arGetLabelingThreshMode(gARHandle, &modea);
		switch (modea) {
		case AR_LABELING_THRESH_MODE_MANUAL:        modea = AR_LABELING_THRESH_MODE_AUTO_MEDIAN; break;
		case AR_LABELING_THRESH_MODE_AUTO_MEDIAN:   modea = AR_LABELING_THRESH_MODE_AUTO_OTSU; break;
		case AR_LABELING_THRESH_MODE_AUTO_OTSU:     modea = AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE; break;
		case AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE: modea = AR_LABELING_THRESH_MODE_AUTO_BRACKETING; break;
		case AR_LABELING_THRESH_MODE_AUTO_BRACKETING:
		default: modea = AR_LABELING_THRESH_MODE_MANUAL; break;
		}
		arSetLabelingThreshMode(gARHandle, modea);
		break;
	case '-':
		threshChange = -5;
		break;
	case '+':
	case '=':
		threshChange = +5;
		break;
	case 'D':
	case 'd':
		arGetDebugMode(gARHandle, &mode);
		arSetDebugMode(gARHandle, !mode);
		break;
	case 's':
	case 'S':
		if (!gARTImageSavePlease) gARTImageSavePlease = TRUE;
		break;
	case '?':
	case '/':
		gShowHelp++;
		if (gShowHelp > 1) gShowHelp = 0;
		break;
	case 'm':
	case 'M':
		gShowMode = !gShowMode;
		break;
	default:
		break;
	}
	if (threshChange) {
		int threshhold;
		arGetLabelingThresh(gARHandle, &threshhold);
		threshhold += threshChange;
		if (threshhold < 0) threshhold = 0;
		if (threshhold > 255) threshhold = 255;
		arSetLabelingThresh(gARHandle, threshhold);
	}

}

static void mainLoop(void)
{
	static int imageNumber = 0;
	static int ms_prev;
	int ms;
	float s_elapsed;
	ARUint8 *image;
	ARdouble err;

	int             j, k;

	// Find out how long since mainLoop() last ran.
	ms = glutGet(GLUT_ELAPSED_TIME);
	s_elapsed = (float)(ms - ms_prev) * 0.001f;
	if (s_elapsed < 0.01f) return; // Don't update more often than 100 Hz.
	ms_prev = ms;

	// Update drawing.
	DrawCubeUpdate(s_elapsed);

	// Grab a video frame.
	if ((image = arVideoGetImage()) != NULL) {
		gARTImage = image;	// Save the fetched image.

		if (gARTImageSavePlease) {
			char imageNumberText[15];
			sprintf(imageNumberText, "image-%04d.jpg", imageNumber++);
			if (arVideoSaveImageJPEG(gARHandle->xsize, gARHandle->ysize, gARHandle->arPixelFormat, gARTImage, imageNumberText, 75, 0) < 0) {
				ARLOGe("Error saving video image.\n");
			}
			gARTImageSavePlease = FALSE;
		}

		gCallCountMarkerDetect++; // Increment ARToolKit FPS counter.

								  // Detect the markers in the video frame.
		if (arDetectMarker(gARHandle, gARTImage) < 0) {
			exit(-3);
		}

		// Check through the marker_info array for highest confidence
		// visible marker matching our preferred pattern.
		k = -1;
		for (j = 0; j < gARHandle->marker_num; j++) {
			if (gARHandle->markerInfo[j].id == gPatt_id) {
				if (k == -1) k = j; // First marker detected.
				else if (gARHandle->markerInfo[j].cf > gARHandle->markerInfo[k].cf) k = j; // Higher confidence marker detected.
			}
		}

		if (k != -1) {
			// Get the transformation between the marker and the real camera into gPatt_trans.
			err = arGetTransMatSquare(gAR3DHandle, &(gARHandle->markerInfo[k]), gPatt_width, gPatt_trans);
			gPatt_found = TRUE;
		}
		else {
			gPatt_found = FALSE;
		}

		// Tell GLUT the display has changed.
		glutPostRedisplay();
	}
}

//
//	This function is called on events when the visibility of the
//	GLUT window changes (including when it first becomes visible).
//
static void Visibility(int visible)
{
	if (visible == GLUT_VISIBLE) {
		glutIdleFunc(mainLoop);
	}
	else {
		glutIdleFunc(NULL);
	}
}

//
//	This function is called when the
//	GLUT window is resized.
//
static void Reshape(int w, int h)
{
	windowWidth = w;
	windowHeight = h;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);

	// Call through to anyone else who needs to know about window sizing here.
}

//
// This function is called when the window needs redrawing.
//
static void Display(void)
{
	ARdouble p[16];
	//ARdouble m[16];

	// Select correct buffer for this context.
	glDrawBuffer(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the buffers for new frame.

	arglPixelBufferDataUpload(gArglSettings, gARTImage);
	arglDispImage(gArglSettings);
	gARTImage = NULL; // Invalidate image data.

					  // Projection transformation.
	arglCameraFrustumRH(&(gCparamLT->param), VIEW_DISTANCE_MIN, VIEW_DISTANCE_MAX, p);
	glMatrixMode(GL_PROJECTION);
#ifdef ARDOUBLE_IS_FLOAT
	glLoadMatrixf(p);
#else
	glLoadMatrixd(p);
#endif
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_DEPTH_TEST);

	// Viewing transformation.
	glLoadIdentity();
	// Lighting and geometry that moves with the camera should go here.
	// (I.e. must be specified before viewing transformations.)
	//none

	if (gPatt_found) {

		// Calculate the camera position relative to the marker.
		// Replace VIEW_SCALEFACTOR with 1.0 to make one drawing unit equal to 1.0 ARToolKit units (usually millimeters).
		arglCameraViewRH((const ARdouble(*)[4])gPatt_trans, m, VIEW_SCALEFACTOR);
#ifdef ARDOUBLE_IS_FLOAT
		glLoadMatrixf(m);
#else
		glLoadMatrixd(m);
#endif

		// All lighting and geometry to be drawn relative to the marker goes here.
		DrawCube();

	} // gPatt_found

	  // Any 2D overlays go here.
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, (GLdouble)windowWidth, 0, (GLdouble)windowHeight, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	//
	// Draw help text and mode.
	//
	if (gShowMode) {
		printMode();
	}
	if (gShowHelp) {
		if (gShowHelp == 1) {
			printHelpKeys();
		}
	}

	glutSwapBuffers();
}

int main(int argc, char** argv)
{
	char glutGamemode[32];
	char cparam_name[] = "C:/Program Files/ARToolKit5/bin/Data/camera_para.dat";
	char vconf[] = "";
	char patt_name[] = "C:/Program Files/ARToolKit5/bin/Data/hiro.patt";

	//
	// Library inits.
	//

	glutInit(&argc, argv);

	//
	// Video setup.
	//

	if (!setupCamera(cparam_name, vconf, &gCparamLT, &gARHandle, &gAR3DHandle)) {
		ARLOGe("main(): Unable to set up AR camera.\n");
		exit(-10);
	}

	//
	// Graphics setup.
	//

	// Set up GL context(s) for OpenGL to draw into.
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	if (!windowed) {
		if (windowRefresh) sprintf(glutGamemode, "%ix%i:%i@%i", windowWidth, windowHeight, windowDepth, windowRefresh);
		else sprintf(glutGamemode, "%ix%i:%i", windowWidth, windowHeight, windowDepth);
		glutGameModeString(glutGamemode);
		glutEnterGameMode();
	}
	else {
		glutInitWindowSize(windowWidth, windowHeight);
		glutCreateWindow(argv[0]);
	}

	// Setup ARgsub_lite library for current OpenGL context.
	if ((gArglSettings = arglSetupForCurrentContext(&(gCparamLT->param), arVideoGetPixelFormat())) == NULL) {
		ARLOGe("main(): arglSetupForCurrentContext() returned error.\n");
		cleanup();
		exit(-4);
	}
	arglSetupDebugMode(gArglSettings, gARHandle);
	arUtilTimerReset();

	// Load marker(s).
	if (!setupMarker(patt_name, &gPatt_id, gARHandle, &gARPattHandle)) {
		ARLOGe("main(): Unable to set up AR marker.\n");
		cleanup();
		exit(-2);
	}

	////load the sculpture
	//	if (!loadOBJ("D:/ARProject/resources/models/UVSphere.obj", sculpt_mod.vertices,sculpt_mod.normals,sculpt_mod.UVs))
	//		exit(-1);
	//	sculpt_mod.computeTangentBasis();
	
		//buffers::Load(sculpt_mod);

	// Register GLUT event-handling callbacks.
	// NB: mainLoop() is registered by Visibility.
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutVisibilityFunc(Visibility);
	glutKeyboardFunc(Keyboard);
	glewInit();

	loadObject();
	loadTexture();
	loadShaders();

	glutMainLoop();

	return (0);
}

//
// The following functions provide the onscreen help text and mode info.
//

static void print(const char *text, const float x, const float y, int calculateXFromRightEdge, int calculateYFromTopEdge)
{
	int i, len;
	GLfloat x0, y0;

	if (!text) return;

	if (calculateXFromRightEdge) {
		x0 = windowWidth - x - (float)glutBitmapLength(GLUT_BITMAP_HELVETICA_10, (const unsigned char *)text);
	}
	else {
		x0 = x;
	}
	if (calculateYFromTopEdge) {
		y0 = windowHeight - y - 10.0f;
	}
	else {
		y0 = y;
	}
	glRasterPos2f(x0, y0);

	len = (int)strlen(text);
	for (i = 0; i < len; i++) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, text[i]);
}

static void drawBackground(const float width, const float height, const float x, const float y)
{
	GLfloat vertices[4][2];

	vertices[0][0] = x; vertices[0][1] = y;
	vertices[1][0] = width + x; vertices[1][1] = y;
	vertices[2][0] = width + x; vertices[2][1] = height + y;
	vertices[3][0] = x; vertices[3][1] = height + y;
	glLoadIdentity();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glEnableClientState(GL_VERTEX_ARRAY);
	glColor4f(0.0f, 0.0f, 0.0f, 0.5f);	// 50% transparent black.
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); // Opaque white.
									   //glLineWidth(1.0f);
									   //glDrawArrays(GL_LINE_LOOP, 0, 4);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_BLEND);
}

static void printHelpKeys()
{
	int i;
	GLfloat  w, bw, bh;
	const char *helpText[] = {
		"Keys:\n",
		" ? or /        Show/hide this help.",
		" q or [esc]    Quit program.",
		" d             Activate / deactivate debug mode.",
		" m             Toggle display of mode info.",
		" a             Toggle between available threshold modes.",
		" - and +       Switch to manual threshold mode, and adjust threshhold up/down by 5.",
		" x             Change image processing mode.",
		" c             Calulcate frame rate.",
	};
#define helpTextLineCount (sizeof(helpText)/sizeof(char *))

	bw = 0.0f;
	for (i = 0; i < helpTextLineCount; i++) {
		w = (float)glutBitmapLength(GLUT_BITMAP_HELVETICA_10, (unsigned char *)helpText[i]);
		if (w > bw) bw = w;
	}
	bh = helpTextLineCount * 10.0f /* character height */ + (helpTextLineCount - 1) * 2.0f /* line spacing */;
	drawBackground(bw, bh, 2.0f, 2.0f);

	for (i = 0; i < helpTextLineCount; i++) print(helpText[i], 2.0f, (helpTextLineCount - 1 - i)*12.0f + 2.0f, 0, 0);;
}

static void printMode()
{
	int len, thresh, line, mode, xsize, ysize;
	AR_LABELING_THRESH_MODE threshMode;
	ARdouble tempF;
	char text[256], *text_p;

	glColor3ub(255, 255, 255);
	line = 1;

	// Image size and processing mode.
	arVideoGetSize(&xsize, &ysize);
	arGetImageProcMode(gARHandle, &mode);
	if (mode == AR_IMAGE_PROC_FRAME_IMAGE) text_p = "full frame";
	else text_p = "even field only";
	snprintf(text, sizeof(text), "Processing %dx%d video frames %s", xsize, ysize, text_p);
	print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
	line++;

	// Threshold mode, and threshold, if applicable.
	arGetLabelingThreshMode(gARHandle, &threshMode);
	switch (threshMode) {
	case AR_LABELING_THRESH_MODE_MANUAL: text_p = "MANUAL"; break;
	case AR_LABELING_THRESH_MODE_AUTO_MEDIAN: text_p = "AUTO_MEDIAN"; break;
	case AR_LABELING_THRESH_MODE_AUTO_OTSU: text_p = "AUTO_OTSU"; break;
	case AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE: text_p = "AUTO_ADAPTIVE"; break;
	case AR_LABELING_THRESH_MODE_AUTO_BRACKETING: text_p = "AUTO_BRACKETING"; break;
	default: text_p = "UNKNOWN"; break;
	}
	snprintf(text, sizeof(text), "Threshold mode: %s", text_p);
	if (threshMode != AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE) {
		arGetLabelingThresh(gARHandle, &thresh);
		len = (int)strlen(text);
		snprintf(text + len, sizeof(text) - len, ", thresh=%d", thresh);
	}
	print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
	line++;

	// Border size, image processing mode, pattern detection mode.
	arGetBorderSize(gARHandle, &tempF);
	snprintf(text, sizeof(text), "Border: %0.1f%%", tempF*100.0);
	arGetPatternDetectionMode(gARHandle, &mode);
	switch (mode) {
	case AR_TEMPLATE_MATCHING_COLOR: text_p = "Colour template (pattern)"; break;
	case AR_TEMPLATE_MATCHING_MONO: text_p = "Mono template (pattern)"; break;
	case AR_MATRIX_CODE_DETECTION: text_p = "Matrix (barcode)"; break;
	case AR_TEMPLATE_MATCHING_COLOR_AND_MATRIX: text_p = "Colour template + Matrix (2 pass, pattern + barcode)"; break;
	case AR_TEMPLATE_MATCHING_MONO_AND_MATRIX: text_p = "Mono template + Matrix (2 pass, pattern + barcode "; break;
	default: text_p = "UNKNOWN"; break;
	}
	len = (int)strlen(text);
	//snprintf(text + len, sizeof(text) - len, ", Pattern detection mode: %s", text_p);
	print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
	line++;

	// Window size.
	snprintf(text, sizeof(text), "Drawing into %dx%d window", windowWidth, windowHeight);
	print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
	line++;

}
