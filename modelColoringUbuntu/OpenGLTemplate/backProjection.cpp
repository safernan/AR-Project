#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int bins = 55;
int histSize[] = { bins, bins };
int channels[] = { 0, 0 };
float h_ranges[] = { 0, 180 };
float s_ranges[] = { 0, 256 };
float v_ranges[] = { 0, 256 };
const float* ranges[] = { h_ranges, s_ranges, v_ranges };
MatND hist;

void computeHistogram(){
    Mat src, hsv, hue;
    src = imread("blueCones.jpg");
    cvtColor(src, hsv, CV_BGR2HSV);
    calcHist(&hsv, 1, channels, Mat(), hist, 2, histSize, ranges);
    normalize(hist, hist, 1.0);
}
//returns black and white image from backprojection
cv::Mat backproject(Mat& img){
	Mat hsvVid, hue, backproject, ROI;
	cvtColor(img, hsvVid, CV_BGR2HSV);
    hue.create( hsvVid.size(), hsvVid.depth() );
	mixChannels( &hsvVid, 1, &hue, 1, channels, 1 );     
    calcBackProject(&hue, 1, channels, hist, backproject, ranges, 255.0);
	for (int i = 1; i < 3; i = i + 2)
		GaussianBlur(backproject, backproject, Size(i, i), 0, 0);
        threshold(backproject, ROI, 50, 255, CV_THRESH_BINARY);
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        vector<int> selected;
        // Find contours
        findContours(ROI, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
        Mat contourImage(ROI.size(), CV_8UC1, cv::Scalar(0));
        for (int idx = 0; idx < contours.size(); idx++) {
            Rect r = boundingRect(contours[idx]);
            if(r.area() > 1200)
            {
				selected.push_back(idx);
                drawContours(contourImage, contours, (int)idx, Scalar(255), FILLED, 8, vector<Vec4i>(), 0, Point());
			}
        }
        imshow("frame", contourImage);
		return contourImage;
}
