#pragma once
#include <glm/vec3.hpp>
#include "CollisionDetection.h"
#include "model.h"

struct contact {
	glm::vec3 contactPoint;
	glm::ivec3 triangle;
	model* mod;
};

namespace distance {
	float point2point(glm::vec3 p1, glm::vec3 p2);
	float point2edge(glm::vec3 p, glm::vec3 p1, glm::vec3 p2);
	float point2plane(glm::vec3 p, glm::vec3 normal, glm::vec3 p1);
};

namespace feature {
	enum FEATURE {
		P1, P2, P3, P12, P23, P31, P123
	};
	glm::vec3 point2triangle(glm::vec3 p, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, FEATURE& feat);
};

namespace collision {
	bool point2model(glm::vec3& p, model& mod, contact* c);
}

