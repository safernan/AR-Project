#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

bool showImg = false;

Mat depth(Mat& img, Mat M, Mat P){//return the vector 3 that correspond to finger position
	//1 compute mean
	Vec2f m (0,0);
	int area = 0;
	for (int i = 0; i < img.cols; ++i)
		for (int j = 0; j < img.rows; ++j){
			if (img.at<unsigned char>(j,i) == 255)
			{ 
				area+=1;
				m += Vec2f(i,j);
			}
		}
	m = m/(float)area;
	//2 compute covariance
	Mat c = Mat::zeros(2, 2, CV_32F);
	for (int i = 0; i < img.cols; ++i)
		for (int j = 0; j < img.rows; ++j){
			if (img.at<unsigned char>(j,i) == 255){
				Mat x = Mat(Vec2f(i,j)-m);
				Mat xt; transpose(x,xt);
				c += x*xt;
			}
		}
	c = c/area;
	//3 compute principal axes
	Mat d, u, vt;
	SVD::compute(c,d,u,vt);
	Vec2f x1 = u.col(0); normalize(x1);
	Vec2f x2 = u.col(1); normalize(x2);
	Mat checkAxes;
	img.copyTo(checkAxes);
	line (checkAxes, Point2i(m), Point2i(m+100*x1), Scalar(100), 2);
	line (checkAxes, Point2i(m), Point2i(m+100*x2), Scalar(100), 2);
	if (showImg){
		imshow("axes", checkAxes);
	}
	//4 get x' and y' width and length and draw bounding box
	float xmax = 0, ymax = 0, xmin = 0, ymin = 0;
	Vec4f p1, p2;
	for (int i = 0; i < img.cols; ++i)
		for (int j = 0; j < img.rows; ++j){
			if (img.at<unsigned char>(j,i) == 255){
				Vec2f v = Vec2f(i,j)-m;
				float x = v.dot(x1);
				float y = v.dot(x2);
				if (x < xmin)
					xmin = x;
				if (y < ymin){
					ymin = y;
					p1 = Vec4f(i,j,1,1);
				}
				if (x > xmax)
					xmax = x;
				if (y > ymax)
					ymax = y;
			}
		}
	float w = xmax-xmin;
	float h = ymax-ymin;
	Vec2f centroid = m + (xmax+xmin)/2.0*x1 + (ymax+ymin)/2.0*x2;
	Mat checkBox;
	img.copyTo(checkBox);
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1+h/2.0*x2), Scalar(100), 2);
	line (checkBox, Point2i(centroid+w/2.0*x1+h/2.0*x2), Point2i(centroid+w/2.0*x1-h/2.0*x2), Scalar(100), 2);
	line (checkBox, Point2i(centroid+w/2.0*x1-h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);
	line (checkBox, Point2i(centroid-w/2.0*x1+h/2.0*x2), Point2i(centroid-w/2.0*x1-h/2.0*x2), Scalar(100), 2);
	if (true || showImg){
		imshow("box", checkBox);
	}
	p2 = p1+h*Vec4f(x2[0], x2[1], 0, 0);
	//5 assume that h = 2*radius of the thimble (for the moment, could be improved if we use more colours)
	float r = 1.8; //18mm
	Mat P1_hom = M.inv()*P.inv()*Mat(p1);
	Mat P2_hom = M.inv()*P.inv()*Mat(p2);
	float dist = sqrt((P1_hom-P2_hom).dot(P1_hom-P2_hom));
	Mat P1 = P1_hom*r/dist;
	Mat P2 = P2_hom*r/dist;
	return (P1+P2)/2;
}
