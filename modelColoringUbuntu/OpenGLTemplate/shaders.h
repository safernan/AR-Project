#pragma once
#include <tuple>
#include <vector>
#include <string>
//#define GLEW_STATIC
#include <GL/glew.h>
#include "textfile.h"
namespace shaders{
	void loadShaders(std::vector<std::tuple<std::string,std::string>> shaderPaths,std::vector<GLuint>& shaderIDs);

};
