#pragma once
#ifdef __unix__ 
    #define OS_Windows 0 //for ubuntu
    //typedef const wchar_t* LPCWSTR;
#elif defined(_WIN32) || defined(WIN32)     /* _Win32 is usually defined by compilers targeting 32 or   64 bit Windows systems */
    #define OS_Windows 1
    #include <windows.h>
#endif
#include <string>
#include <iostream>
#include "debug.h"
//if (!OS_WINDOWS)
void printConsole(std::string& str)
{
	//std::wstring wstr = std::wstring(str.begin(), str.end());
	//LPCWSTR lpcwstr = wstr.c_str();
	std::cout << str << std::endl;
}

void debugShader(GLuint shader) {
	int maxLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	std::string errorLog; errorLog.resize(maxLength);
	glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);
	printConsole(errorLog);
}

