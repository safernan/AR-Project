#ifdef __unix__
#define OS_Windows 0
#elif __APPLE__
#define OS_Windows 0
#elif defined(_WIN32) || defined(WIN32)     /* _Win32 is usually defined by compilers targeting 32 or   64 bit Windows systems */
#define OS_Windows 1
#include <windows.h>
#endif
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <string>
#include <tuple>

#include <GL/glew.h>
#if __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#if __unix__
#include <GL/freeglut.h>
#endif



#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <AR/config.h>
#include <AR/video.h>
#include <AR/param.h>			// arparamdisp()
#include <AR/ar.h>
#include <AR/gsub_lite.h>

#include "textfile.h"
#include "AuxilaryIO.h"
#include "VBO.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instance.h"

#include "global.h"
#include "CollisionDetection.h"

#include "backProjection.h"
#include "depthEstimation.h"
#include "HandSegmentation.h"
#include "ForegroundDetection.h"

cv::VideoWriter writer;
//bool write = true;
//models
model sculpt_mod;
model finger_mod;
model projection_mod;
//materials
material sculpt_mat;
material finger_mat;
material projectionBackground_mat;
material projectionHand_mat;

//instances
instance sculpt_inst;
instance finger_inst;
instance projectionBackground_inst;
instance projectionHand_inst;
//translation of the model
glm::vec3 sculpt_translationOffset = glm::vec3(0.0f);
glm::vec3 finger_translationOffset = glm::vec3(0.367f,0.081f,-1.05f);
glm::vec3 global_collision_point = glm::vec3(0.0f);
float point2sculptDistance =1;

ARdouble m[16]={1,0,0,0,
    0,1,0,0,
    0,0,1,0,
    0,0,0,1};

bool handOverGraphics=false;
//
////OpenCV
cv::Mat textureMask;
cv::Mat brushMask;
cv::Mat fingerMask;
cv::Mat projectionBackgroundMask;
////demo variables
std::string imgFold = "/Users/seandillon/cappp";

glm::vec3 barycentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p) {
    float w1 = glm::length(glm::cross(p3 - p, p2 - p));
    float w2 = glm::length(glm::cross(p3 - p, p1 - p));
    float w3 = glm::length(glm::cross(p2 - p, p1 - p));
    float sum = glm::length(glm::cross(p3 - p1, p2 - p1));
    return glm::vec3(w1 / sum, w2 / sum, w3 / sum);
}

glm::vec2 linear_int(glm::vec3 p1,glm::vec3 p2,glm::vec3 p){
    float w1 = glm::length(p-p2);
    float w2 = glm::length(p-p1);
    float sum = glm::length(p2-p1);
    return glm::vec2(w1/sum,w2/sum);
}


void handlephysics() {
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(glm::transpose(finger_inst.transform), scale, rotation, translation, skew, perspective);
    translation = glm::vec3(finger_inst.transform[3])/(finger_inst.transform[3][3]);
    //check if the finger collides with the model
    contact c;
    glm::vec3 np;
    glm::vec3 npn=glm::vec3(0,0,-1);
    glm::vec3 lineColor=glm::vec3(1,1,1);
    if (collision::point2instance(translation, sculpt_inst, &c,&np,&npn)) {
        if(!c.backFace){
            lineColor=glm::vec3(1,0,0);
            //global_collision_point= glm::vec3(sculpt_inst.transform*glm::vec4(c.contactPoint,1.0f));
            //interpolate the uv coords to find the values for the contact point
            glm::vec2 uvs;
            if(c.feat==feature::P123){
                glm::vec3 weights = barycentric(c.inst->mod->vertices[c.triangle.x], c.inst->mod->vertices[c.triangle.y], c.inst->mod->vertices[c.triangle.z],c.contactPoint);
                uvs = c.inst->mod->UVs[c.triangle.x] * weights.x + c.inst->mod->UVs[c.triangle.y] * weights.y + c.inst->mod->UVs[c.triangle.z] * weights.z;
                // std::cout<<"1:weights are "<<glm::to_string(weights)<<std::endl;
            }
            
            else if(c.feat==feature::P12){
                glm::vec2 weights = linear_int(c.inst->mod->vertices[c.triangle.x],c.inst->mod->vertices[c.triangle.y], c.contactPoint);
                uvs = c.inst->mod->UVs[c.triangle.x] * weights.x + c.inst->mod->UVs[c.triangle.y] * weights.y;
                //std::cout<<"2:weights are "<<glm::to_string(weights)<<std::endl;
            }
            else if(c.feat==feature::P23){
                glm::vec2 weights = linear_int(c.inst->mod->vertices[c.triangle.y],c.inst->mod->vertices[c.triangle.z], c.contactPoint);
                uvs = c.inst->mod->UVs[c.triangle.y] * weights.x + c.inst->mod->UVs[c.triangle.z] * weights.y;
                //std::cout<<"3:weights are "<<glm::to_string(weights)<<std::endl;
            }
            else if(c.feat==feature::P31){
                glm::vec2 weights = linear_int(c.inst->mod->vertices[c.triangle.z],c.inst->mod->vertices[c.triangle.x], c.contactPoint);
                uvs = c.inst->mod->UVs[c.triangle.z] * weights.x + c.inst->mod->UVs[c.triangle.x] * weights.y;
                // std::cout<<"4:weights are "<<glm::to_string(weights)<<std::endl;
            }
            else if(c.feat==feature::P1){
                uvs = c.inst->mod->UVs[c.triangle.x];
            }
            else if(c.feat==feature::P2){
                uvs = c.inst->mod->UVs[c.triangle.y];
            }
            else if(c.feat==feature::P3){
                uvs = c.inst->mod->UVs[c.triangle.z];
            }
            
            int text_size = textureMask.cols;
            int kernel_size = brushMask.cols;
            for (int i = 0; i < kernel_size; i++) {
                for (int j = 0; j < kernel_size; j++) {
                    textureMask.at<cv::Vec3b>((text_size - 1) * (uvs.y)+i, (text_size - 1) * (uvs.x)+j) =
                    textureMask.at<cv::Vec3b>((text_size - 1) * (uvs.y) + i, (text_size - 1) * (uvs.x) + j)*(1- brushMask.at<cv::Vec3b>(i, j)[0] / 255.0f)+
                    cv::Vec3b(255, 0, 0)*(brushMask.at<cv::Vec3b>(i,j)[0]/255.0f);
                }
            }
            
            sculpt_mat.DiffuseMapID = textures::loadTexture(textureMask,sculpt_mat.DiffuseMapID);
        }
    }
    handOverGraphics=true;
    if(glm::dot(glm::vec3(sculpt_inst.transform*glm::vec4(npn,0)),glm::vec3(0,0,1))<0){
        handOverGraphics=false;
    }
    global_collision_point =glm::vec3(sculpt_inst.transform*glm::vec4(np,1.0f));
    
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);	glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
    glColor3f(lineColor.x,lineColor.y,lineColor.z);
    glLineWidth(5.0f);
    glBegin(GL_LINES);
    glVertex3f(translation.x,translation.y,translation.z);
    glVertex3f(global_collision_point.x,global_collision_point.y,global_collision_point.z);
    glEnd();
    
    
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    //calculate the distance between finger and collision point
    point2sculptDistance = glm::distance(translation,global_collision_point);
    
}

void processSpecialKeys(int key, int x, int y) {
    switch (key)
    {
        case GLUT_KEY_RIGHT:
            controls::Key_Right = true;
            break;
        case GLUT_KEY_LEFT:
            controls::Key_Left = true;
            break;
        case GLUT_KEY_UP:
            controls::Key_Up = true;
            break;
        case GLUT_KEY_DOWN:
            controls::Key_Down = true;
            break;
        default:
            break;
    }
    glutPostRedisplay();
}
//
void processSpecialKeysUp(int key, int x, int y) {
    switch (key)
    {
        case GLUT_KEY_RIGHT:
            controls::Key_Right = false;
            break;
        case GLUT_KEY_LEFT:
            controls::Key_Left = false;
            break;
        case GLUT_KEY_UP:
            controls::Key_Up = false;
            break;
        case GLUT_KEY_DOWN:
            controls::Key_Down = false;
            break;
        default:
            break;
    }
}

void processNormalKeys(unsigned char key, int x, int y) {
    static float zoom = 45.0;
    switch (key)
    {
        case '8':
            controls::Key_8 = true;
            break;
        case '2':
            controls::Key_2 = true;
            break;
        case '4':
            controls::Key_4 = true;
            break;
        case '6':
            controls::Key_6 = true;
            break;
        case '7':
            controls::Key_7 = true;
            break;
        case '9':
            controls::Key_9 = true;
            break;
        case '0':
            exit(0);
        default:
            break;
    }
    glutPostRedisplay();
}

void processNormalKeysUp(unsigned char key, int x, int y) {
    static float zoom = 45.0;
    switch (key)
    {
        case '8':
            controls::Key_8 = false;
            break;
        case '2':
            controls::Key_2 = false;
            break;
        case '4':
            controls::Key_4 = false;
            break;
        case '6':
            controls::Key_6 = false;
            break;
        case '7':
            controls::Key_7 = false;
            break;
        case '9':
            controls::Key_9 = false;
            break;
        default:
            break;
    }
    glutPostRedisplay();
}

void loadTexture() {
    if(OS_Windows){
        textureMask = cv::imread("D:/ARProject/resources/textures/white.png");
        projectionBackgroundMask = cv::imread("D:/ARProject/resources/textures/testUV2.png");
        sculpt_mat.DiffuseMapID = textures::loadTexture(textureMask);
        projectionBackground_mat.DiffuseMapID = textures::loadTexture(projectionBackgroundMask);
        brushMask = cv::imread("D:/ARProject/resources/brushes/gauss.jpg");
        fingerMask = cv::imread("D:/ARProject/resources/textures/finger.png");
    }
    else{
#if __APPLE__
        textureMask = cv::imread("./textures/white.png");
        brushMask = cv::imread("./brushes/gauss.jpg");
        fingerMask = cv::imread("./textures/finger.png");
#else
        textureMask = cv::imread("../../resources/textures/white.png");
        brushMask = cv::imread("../../resources/brushes/gauss.jpg");
        fingerMask = cv::imread("../../resources/textures/finger.png");
#endif
        sculpt_mat.DiffuseMapID = textures::loadTexture(textureMask);
        cv::cvtColor(fingerMask,fingerMask,CV_BGR2RGB);
        finger_mat.DiffuseMapID = textures::loadTexture(fingerMask);
        
    }
    
}


void loadObject() {
    //load the sculpture
    if(OS_Windows){
        if (!loadOBJ("D:/ARProject/resources/models/UVSphere.obj", sculpt_mod.vertices,sculpt_mod.normals,sculpt_mod.UVs))
            exit(-1);
    }
    else{
#if __APPLE__
        if (!loadOBJ("./models/UVSphere.obj", sculpt_mod.vertices,sculpt_mod.normals,sculpt_mod.UVs))
            exit(-1);
#else
        if (!loadOBJ("../../resources/models/UVSphere.obj", sculpt_mod.vertices,sculpt_mod.normals,sculpt_mod.UVs))
            exit(-1);
#endif
    }
    buffers::Load(sculpt_mod);
}

//Framebuffer to ARImage
void getScreenShot(IplImage *imgO);

#include <stdio.h>
#include <string.h>
#ifdef _WIN32
#  define snprintf _snprintf
#endif
#include <stdlib.h>					// malloc(), free()
#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif
#include <AR/config.h>
#include <AR/video.h>
#include <AR/param.h>			// arParamDisp()
#include <AR/ar.h>
#include <AR/gsub_lite.h>


// ============================================================================
//	Constants
// ============================================================================

#define VIEW_SCALEFACTOR		1.0         // Units received from ARToolKit tracking will be multiplied by this factor before being used in OpenGL drawing.
#define VIEW_DISTANCE_MIN		40.0        // Objects closer to the camera than this will not be displayed. OpenGL units.
#define VIEW_DISTANCE_MAX		10000.0     // Objects further away from the camera than this will not be displayed. OpenGL units.

// ============================================================================
//	Global variables
// ============================================================================

// Preferences.
static int windowed = TRUE;                     // Use windowed (TRUE) or fullscreen mode (FALSE) on launch.
static int windowWidth = 640;					// Initial window width, also updated during program execution.
static int windowHeight = 480;                  // Initial window height, also updated during program execution.
static int windowDepth = 32;					// Fullscreen mode bit depth.
static int windowRefresh = 0;					// Fullscreen mode refresh rate. Set to 0 to use default rate.

// Image acquisition.
static ARUint8		*gARTImage = NULL;
static int          gARTImageSavePlease = FALSE;

// Marker detection.
static ARHandle		*gARHandle = NULL;
static ARPattHandle	*gARPattHandle = NULL;
static long			gCallCountMarkerDetect = 0;

// Transformation matrix retrieval.
static AR3DHandle	*gAR3DHandle = NULL;
static ARdouble		gPatt_width = 80.0;	// Per-marker, but we are using only 1 marker.
static ARdouble		gPatt_trans[3][4];		// Per-marker, but we are using only 1 marker.
static int			gPatt_found = FALSE;	// Per-marker, but we are using only 1 marker.
static int			gPatt_id;				// Per-marker, but we are using only 1 marker.

// Drawing.
static ARParamLT *gCparamLT = NULL;
static ARGL_CONTEXT_SETTINGS_REF gArglSettings = NULL;
static int gShowHelp = 1;
static int gShowMode = 1;
static int gDrawRotate = FALSE;
static float gDrawRotateAngle = 0;			// For use in drawing.


static cv::Mat imgOpenCV;  //SMART POINTERS!!!!
cv::Mat glove;
IplImage* imgTest;
IplImage* imgTest2;
cv::Mat fingerPos;
cv::MatND mHistogram;
int mNumberChannels;
int* mChannelNumbers;
int* mNumberBins;
float mChannelRange[2];
const float* channel_ranges[] = { mChannelRange, mChannelRange, mChannelRange };


// ============================================================================
//	Function prototypes.
// ============================================================================

static void print(const char *text, const float x, const float y, int calculateXFromRightEdge, int calculateYFromTopEdge);
static void drawBackground(const float width, const float height, const float x, const float y);
static void printHelpKeys();
static void printMode();

// ============================================================================
//	Functions
// ============================================================================

void DinosaurRender(instance& inst) {
    //**GRANPA DRAWING**//
    glPushMatrix(); // Save world coordinate system.
    ARdouble mat[16];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            mat[i * 4 + j] = inst.transform[i][j];
        }
    }
    glLoadMatrixd(mat);
    //glDisable(GL_LIGHTING);
    //glDisable(GL_BLEND);
    inst.mat->BindTextures();
    glVertexPointer(3, GL_FLOAT, 0, &inst.mod->vertices[0]);
    glEnableClientState(GL_VERTEX_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, 0, &inst.mod->UVs[0]);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glLineWidth(0.1f);
    //   glPolygonMode(	GL_FRONT_AND_BACK,GL_LINE);
    //	glDrawArrays(GL_TRIANGLES, 0, inst.mod->vertices.size());
    glPolygonMode(	GL_FRONT_AND_BACK,GL_FILL);
    glDrawArrays(GL_TRIANGLES, 0, inst.mod->vertices.size());
    glPopMatrix();
    //**//
}

// Something to look at, draw a rotating colour cube.
static void DrawCube(void)
{
    glm::mat4 matrix = glm::mat4(
                                 m[0], m[1], m[2], m[3],
                                 m[4], m[5], m[6], m[7],
                                 m[8], m[9], m[10], m[11],
                                 m[12], m[13], m[14], m[15]
                                 );
    
    sculpt_inst.transform =matrix
    *glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f))
    *glm::scale(glm::mat4(1.0f),glm::vec3(40.0f, 40.0f, 40.0f));
    
    static float depthh = -1.0f;
    float speed=10.0f;
    if (controls::Key_Up) {
        sculpt_translationOffset += speed*glm::vec3(0,0, dtime::deltaTime);
        depthh += 0.01f;
    }
    if (controls::Key_Down) {
        sculpt_translationOffset += speed*glm::vec3(0,0, -dtime::deltaTime);
        depthh -= 0.01f;
        
    }
    if (controls::Key_Left) {
        sculpt_translationOffset += speed*glm::vec3(-dtime::deltaTime, 0, 0);
    }
    if (controls::Key_Right) {
        sculpt_translationOffset += speed*glm::vec3(dtime::deltaTime, 0, 0);
    }
    if (controls::Key_8) {
        finger_translationOffset += speed*glm::vec3(0, 0, -dtime::deltaTime);
    }
    if (controls::Key_2) {
        finger_translationOffset += speed*glm::vec3(0, 0, dtime::deltaTime);
    }
    if (controls::Key_4) {
        finger_translationOffset += speed*glm::vec3(-dtime::deltaTime, 0, 0);
    }
    if (controls::Key_6) {
        finger_translationOffset += speed*glm::vec3(dtime::deltaTime, 0, 0);
    }
    if (controls::Key_9) {
        finger_translationOffset += speed*glm::vec3(0, dtime::deltaTime, 0);
    }
    if (controls::Key_7) {
        finger_translationOffset += speed*glm::vec3(0, -dtime::deltaTime, 0);
    }
    glm::vec3 old_finger_pos= finger_inst.transform[3];
    glm::vec3 finger_transl = glm::vec3(fingerPos.at<float>(0, 0), fingerPos.at<float>(0, 1), -1);
    glm::mat4 f = glm::inverse(camera::ProjectionMatrix)*glm::translate(glm::mat4(1.0f),finger_transl);
    f[3][0] = fingerPos.at<float>(0, 2)*fingerPos.at<float>(0, 0) / camera::ProjectionMatrix[0][0];
    f[3][1] = fingerPos.at<float>(0, 2)*fingerPos.at<float>(0, 1) / camera::ProjectionMatrix[1][1];
    f[3][2] = -fingerPos.at<float>(0, 2);
    finger_inst.transform = f*glm::scale(glm::mat4(1.0f), glm::vec3(0.5, 0.5, 0.5));
    //
    DinosaurRender(sculpt_inst);
    finger_inst.transform /= finger_inst.transform[3][3];
    finger_inst.transform = finger_inst.transform*glm::scale(glm::mat4(1.0f),glm::vec3(0.5,0.5,0.5));
    glm::vec3 new_finger_pos = finger_inst.transform[3];
    
    float maxDist = glm::distance(new_finger_pos,old_finger_pos);
    float conditionDist = maxDist/(0.0001+glm::abs(sculpt_inst.transform[3][3]));
    if(conditionDist<500000000.0f)
    {
        glm::vec3 direction  = glm::normalize(new_finger_pos-old_finger_pos);
        static int counter=0;
        float speedd=0;
        if(point2sculptDistance==point2sculptDistance){
            speedd=glm::min(glm::max(0.5f*point2sculptDistance,2.5f),maxDist);
            speedd=glm::min(speedd,glm::min(glm::max(0.0005f*point2sculptDistance*point2sculptDistance,2.5f),maxDist));
        }
        else{
            speedd=maxDist;
        }
        
        
        if(old_finger_pos[0]==old_finger_pos[0]){
            finger_inst.transform[3]=glm::vec4(old_finger_pos+speedd*direction,finger_inst.transform[3][3]);
        }
        
    }
    
    //@TODO REMOVE AFTER TEST
   // finger_inst.transform = glm::translate(glm::mat4(1.0f),sculpt_translationOffset)*sculpt_inst.transform*glm::scale(glm::mat4(1.0f),glm::vec3(0.1,0.1,0.1));
    DinosaurRender(finger_inst);
    //get the framebuffer after rendering to a openCV format
    getScreenShot(imgTest2);
    cv::Mat frameBuffer = cv::cvarrToMat(imgTest2);
    if(handOverGraphics){
        for (int row = 0; row < glove.rows; row++)
        {
            for (int col = 0; col < glove.cols; col++)
            {
                cv::Vec4b pixel = glove.at<cv::Vec4b>(row, col);
                if (pixel[0]!=0)//its not black
                {
                    frameBuffer.at<cv::Vec4b>(row,col)*=0.5f;
                    frameBuffer.at<cv::Vec4b>(row,col)+=0.5f*pixel;
                }
            }
        }
    }
    cv::imshow("Final Result",frameBuffer);
    cv::Mat RGBframeBuffer;
    cv::cvtColor(frameBuffer,RGBframeBuffer,CV_BGR2RGB);
    writer<<frameBuffer;
}

static void DrawCubeUpdate(float timeDelta)
{
    return;
    if (gDrawRotate) {
        gDrawRotateAngle += timeDelta * 45.0f; // Rotate cube at 45 degrees per second.
        if (gDrawRotateAngle > 360.0f) gDrawRotateAngle -= 360.0f;
    }
}

static int setupCamera(const char *cparam_name, char *vconf, ARParamLT **cparamLT_p, ARHandle **arhandle, AR3DHandle **ar3dhandle)
{
    ARParam			cparam;
    int				xsize, ysize;
    AR_PIXEL_FORMAT pixFormat;
    //cv::namedWindow("kay");
    // Open the video path.
    if (arVideoOpen(vconf) < 0) {
        ARLOGe("setupCamera(): Unable to open connection to camera.\n");
        return (FALSE);
    }
    
    // Find the size of the window.
    if (arVideoGetSize(&xsize, &ysize) < 0) {
        ARLOGe("setupCamera(): Unable to determine camera frame size.\n");
        arVideoClose();
        return (FALSE);
    }
    ARLOGi("Camera image size (x,y) = (%d,%d)\n", xsize, ysize);
    
    // Get the format in which the camera is returning pixels.
    pixFormat = arVideoGetPixelFormat();
    if (pixFormat == AR_PIXEL_FORMAT_INVALID) {
        ARLOGe("setupCamera(): Camera is using unsupported pixel format.\n");
        arVideoClose();
        return (FALSE);
    }
    
    // Load the camera parameters, resize for the window and init.
    if (arParamLoad(cparam_name, 1, &cparam) < 0) {
        ARLOGe("setupCamera(): Error loading parameter file %s for camera.\n", cparam_name);
        arVideoClose();
        return (FALSE);
    }
    if (cparam.xsize != xsize || cparam.ysize != ysize) {
        ARLOGw("*** Camera Parameter resized from %d, %d. ***\n", cparam.xsize, cparam.ysize);
        arParamChangeSize(&cparam, xsize, ysize, &cparam);
    }
#ifdef DEBUG
    ARLOG("*** Camera Parameter ***\n");
    arParamDisp(&cparam);
#endif
    if ((*cparamLT_p = arParamLTCreate(&cparam, AR_PARAM_LT_DEFAULT_OFFSET)) == NULL) {
        ARLOGe("setupCamera(): Error: arParamLTCreate.\n");
        return (FALSE);
    }
    
    if ((*arhandle = arCreateHandle(*cparamLT_p)) == NULL) {
        ARLOGe("setupCamera(): Error: arCreateHandle.\n");
        return (FALSE);
    }
    if (arSetPixelFormat(*arhandle, pixFormat) < 0) {
        ARLOGe("setupCamera(): Error: arSetPixelFormat.\n");
        return (FALSE);
    }
    if (arSetDebugMode(*arhandle, AR_DEBUG_DISABLE) < 0) {
        ARLOGe("setupCamera(): Error: arSetDebugMode.\n");
        return (FALSE);
    }
    if ((*ar3dhandle = ar3DCreateHandle(&cparam)) == NULL) {
        ARLOGe("setupCamera(): Error: ar3DCreateHandle.\n");
        return (FALSE);
    }
    
    if (arVideoCapStart() != 0) {
        ARLOGe("setupCamera(): Unable to begin camera data capture.\n");
        return (FALSE);
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////OPENCV
    imgOpenCV = cv::Mat(xsize, ysize, CV_8UC3);
    
    imgTest = cvCreateImage(cvSize(xsize, ysize), IPL_DEPTH_8U, 3);
    
    char file_location[] = "D:/ARProject/resources/samples/";
    if(OS_Windows){
        strcpy(file_location, "D:/ARProject/resources/samples/");
    }
    else{
#if __APPLE__
        strcpy(file_location, "./samples/");
#else
        strcpy(file_location, "../../resources/samples/");
#endif
    }
    char* image_files[] = {
        
        "sample2.png", //0
        "DSC_1432.JPG",
        "DSC_1433.JPG"
    };
    
    // Load images
    int number_of_images = sizeof(image_files) / sizeof(image_files[0]);
    cv::Mat* image = new cv::Mat[number_of_images];
    for (int file_no = 0; (file_no < number_of_images); file_no++)
    {
        std::string filename(file_location);
        filename.append(image_files[file_no]);
        image[file_no] = cv::imread(filename, -1);
        if (image[file_no].empty())
        {
            std::cout << "Could not open " << image[file_no] << std::endl;
            //return -2;
        }
    }
    
    cv::Mat sample;
    if(OS_Windows)
        sample = cv::imread("D:/ARProject/resources/textures/testUV2.png");
    else
#if __APPLE__
        sample = cv::imread("./textures/testUV2.png");
#else
    sample = cv::imread("../../resources/textures/testUV2.png");
#endif
    cv::Mat hls_image;
    //cv::imshow("OpenCV Image", sample);
    //cv::imshow("OpenCV Image", image[0]);
    cvtColor(image[0], hls_image, CV_BGR2HLS);
    cv::Mat mImage;
    mImage = hls_image;
    mNumberChannels = mImage.channels();
    mChannelNumbers = new int[mNumberChannels];
    mNumberBins = new int[mNumberChannels];
    mChannelRange[0] = 0.0;
    mChannelRange[1] = 255.0;
    
    for (int count = 0; count < mNumberChannels; count++)
    {
        mChannelNumbers[count] = count;
        mNumberBins[count] = 6;
    }
    
    
    calcHist(&mImage, 1, mChannelNumbers, cv::Mat(), mHistogram, mNumberChannels, mNumberBins, channel_ranges);
    normalize(mHistogram, mHistogram, 1.0);
    /////temp crc code
    arVideoGetSize(&xsize, &ysize);
    imgTest2 = cvCreateImage(cvSize(xsize, ysize), IPL_DEPTH_8U, 4);
    ////end crc code
    ///////////////////////////////////////////////////////////////////////////////////////////////OPENCV
    
    
    
    return (TRUE);
}

static int setupMarker(const char *patt_name, int *patt_id, ARHandle *arhandle, ARPattHandle **pattHandle_p)
{
    if ((*pattHandle_p = arPattCreateHandle()) == NULL) {
        ARLOGe("setupMarker(): Error: arPattCreateHandle.\n");
        return (FALSE);
    }
    
    // Loading only 1 pattern in this example.
    if ((*patt_id = arPattLoad(*pattHandle_p, patt_name)) < 0) {
        ARLOGe("setupMarker(): Error loading pattern file %s.\n", patt_name);
        arPattDeleteHandle(*pattHandle_p);
        return (FALSE);
    }
    
    arPattAttach(arhandle, *pattHandle_p);
    
    return (TRUE);
}

static void cleanup(void)
{
    arglCleanup(gArglSettings);
    gArglSettings = NULL;
    arPattDetach(gARHandle);
    arPattDeleteHandle(gARPattHandle);
    arVideoCapStop();
    ar3DDeleteHandle(&gAR3DHandle);
    arDeleteHandle(gARHandle);
    arParamLTFree(&gCparamLT);
    arVideoClose();
}

static void Keyboard(unsigned char key, int x, int y)
{
    processNormalKeys(key,x,y); //for our own control of the objects
    int mode, threshChange = 0;
    AR_LABELING_THRESH_MODE modea;
    
    switch (key) {
        case 0x1B:						// Quit.
        case 'Q':
        case 'q':
            cleanup();
            exit(0);
            break;
        case ' ':
            gDrawRotate = !gDrawRotate;
            break;
        case 'X':
        case 'x':
            arGetImageProcMode(gARHandle, &mode);
            switch (mode) {
                case AR_IMAGE_PROC_FRAME_IMAGE:  mode = AR_IMAGE_PROC_FIELD_IMAGE; break;
                case AR_IMAGE_PROC_FIELD_IMAGE:
                default: mode = AR_IMAGE_PROC_FRAME_IMAGE; break;
            }
            arSetImageProcMode(gARHandle, mode);
            break;
        case 'C':
        case 'c':
            ARLOGe("*** Camera - %f (frame/sec)\n", (double)gCallCountMarkerDetect / arUtilTimer());
            gCallCountMarkerDetect = 0;
            arUtilTimerReset();
            break;
        case 'a':
        case 'A':
            arGetLabelingThreshMode(gARHandle, &modea);
            switch (modea) {
                case AR_LABELING_THRESH_MODE_MANUAL:        modea = AR_LABELING_THRESH_MODE_AUTO_MEDIAN; break;
                case AR_LABELING_THRESH_MODE_AUTO_MEDIAN:   modea = AR_LABELING_THRESH_MODE_AUTO_OTSU; break;
                case AR_LABELING_THRESH_MODE_AUTO_OTSU:     modea = AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE; break;
                case AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE: modea = AR_LABELING_THRESH_MODE_AUTO_BRACKETING; break;
                case AR_LABELING_THRESH_MODE_AUTO_BRACKETING:
                default: modea = AR_LABELING_THRESH_MODE_MANUAL; break;
            }
            arSetLabelingThreshMode(gARHandle, modea);
            break;
        case '-':
            threshChange = -5;
            break;
        case '+':
        case '=':
            threshChange = +5;
            break;
        case 'D':
        case 'd':
            arGetDebugMode(gARHandle, &mode);
            arSetDebugMode(gARHandle, !mode);
            break;
        case 's':
        case 'S':
            if (!gARTImageSavePlease) gARTImageSavePlease = TRUE;
            break;
        case '?':
        case '/':
            gShowHelp++;
            if (gShowHelp > 1) gShowHelp = 0;
            break;
        case 'm':
        case 'M':
            gShowMode = !gShowMode;
            break;
        default:
            break;
    }
    if (threshChange) {
        int threshhold;
        arGetLabelingThresh(gARHandle, &threshhold);
        threshhold += threshChange;
        if (threshhold < 0) threshhold = 0;
        if (threshhold > 255) threshhold = 255;
        arSetLabelingThresh(gARHandle, threshhold);
    }
    
}

static void mainLoop(void)
{
    static int imageNumber = 0;
    static int ms_prev;
    int ms;
    float s_elapsed;
#if __APPLE__
    ARUint8 *image;
#else
    AR2VideoBufferT* image;
#endif
    ARdouble err;
    
    int             j, k;
    
    // Find out how long since mainLoop() last ran.
    ms = glutGet(GLUT_ELAPSED_TIME);
    s_elapsed = (float)(ms - ms_prev) * 0.001f;
    if (s_elapsed < 0.01f) return; // Don't update more often than 100 Hz.
    ms_prev = ms;
    
    // Update drawing.
    DrawCubeUpdate(s_elapsed);
    
    // Grab a video frame.
    
    if ((image = arVideoGetImage()) != NULL) {
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////OPENCV
#if __APPLE__
        imgTest->imageData = (char *)image;
#else
        imgTest->imageData = (char *)image->buff;
#endif
        int xsize;int ysize;
        arVideoGetSize(&xsize, &ysize);
#if __APPLE__
        imgOpenCV = cv::Mat(CvSize(xsize,ysize), CV_8UC4, (void*)image);
#else
        imgOpenCV = cv::Mat(CvSize(xsize,ysize), CV_8UC3, (void*)image->buff);
        cv::cvtColor(imgOpenCV,imgOpenCV,CV_BGR2BGRA);
#endif
        *imgTest = glove;
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        
#if __APPLE__
        gARTImage = image;	// Save the fetched image.
#else
        gARTImage = image->buff;	// Save the fetched image.
#endif
        
        if (gARTImageSavePlease) {
            char imageNumberText[15];
            sprintf(imageNumberText, "image-%04d.jpg", imageNumber++);
            if (arVideoSaveImageJPEG(gARHandle->xsize, gARHandle->ysize, gARHandle->arPixelFormat, gARTImage, imageNumberText, 75, 0) < 0) {
                ARLOGe("Error saving video image.\n");
            }
            gARTImageSavePlease = FALSE;
        }
        
        gCallCountMarkerDetect++; // Increment ARToolKit FPS counter.
        
        // Detect the markers in the video frame.
        if (arDetectMarker(gARHandle, /*gARTImage*/ image) < 0) {
            exit(-3);
        }
        
        // Check through the marker_info array for highest confidence
        // visible marker matching our preferred pattern.
        k = -1;
        for (j = 0; j < gARHandle->marker_num; j++) {
            if (gARHandle->markerInfo[j].id == gPatt_id) {
                if (k == -1) k = j; // First marker detected.
                else if (gARHandle->markerInfo[j].cf > gARHandle->markerInfo[k].cf) k = j; // Higher confidence marker detected.
            }
        }
        
        if (k != -1) {
            // Get the transformation between the marker and the real camera into gPatt_trans.
            err = arGetTransMatSquare(gAR3DHandle, &(gARHandle->markerInfo[k]), gPatt_width, gPatt_trans);
            gPatt_found = TRUE;
        }
        else {
            gPatt_found = FALSE;
        }
        
        // Tell GLUT the display has changed.
        glutPostRedisplay();
    }
}

//
//	This function is called on events when the visibility of the
//	GLUT window changes (including when it first becomes visible).
//
static void Visibility(int visible)
{
    if (visible == GLUT_VISIBLE) {
        glutIdleFunc(mainLoop);
    }
    else {
        glutIdleFunc(NULL);
    }
}

//
//	This function is called when the
//	GLUT window is resized.
//
static void Reshape(int w, int h)
{
    windowWidth = w;
    windowHeight = h;
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    
    // Call through to anyone else who needs to know about window sizing here.
}

//
// This function is called when the window needs redrawing.
//
static void Display(void)
{
    ARdouble p[16];
    //update the deltaTime
    dtime::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
    dtime::deltaTime = ((float)(dtime::currentFrameTime - dtime::previousFrameTime)) / 1000.0;
    dtime::previousFrameTime = dtime::currentFrameTime;
    // Select correct buffer for this context.
    glDrawBuffer(GL_BACK);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the buffers for new frame.
    
    arglPixelBufferDataUpload(gArglSettings, gARTImage);
    arglDispImage(gArglSettings);
    gARTImage = NULL; // Invalidate image data.
    
    // Projection transformation.
    arglCameraFrustumRH(&(gCparamLT->param), VIEW_DISTANCE_MIN, VIEW_DISTANCE_MAX, p);
    camera::ProjectionMatrix = glm::mat4(p[0], p[4], p[8], p[12],
                                         p[1], p[5], p[9], p[13],
                                         p[2], p[6], p[10], p[14],
                                         p[3], p[7], p[11], p[15]
                                         );
    camera::ProjectionMatrix = glm::transpose(camera::ProjectionMatrix);
    glMatrixMode(GL_PROJECTION);
#ifdef ARDOUBLE_IS_FLOAT
    glLoadMatrixf(p);
#else
    glLoadMatrixd(p);
#endif
    glMatrixMode(GL_MODELVIEW);
    
    glEnable(GL_DEPTH_TEST);
    
    // Viewing transformation.
    glLoadIdentity();
    // Lighting and geometry that moves with the camera should go here.
    // (I.e. must be specified before viewing transformations.)
    //none
    if (gPatt_found) {
        // Calculate the camera position relative to the marker.
        // Replace VIEW_SCALEFACTOR with 1.0 to make one drawing unit equal to 1.0 ARToolKit units (usually millimeters).
        arglCameraViewRH((const ARdouble(*)[4])gPatt_trans, m, VIEW_SCALEFACTOR);
        
    } // gPatt_found
    cv::Mat M = cv::Mat(4,4,CV_32F);
    cv::Mat P = cv::Mat(4,4,CV_32F);
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
        {
            M.at<float>(i,j) = m[4*j+i];
            P.at<float>(i,j) = p[4*j+i];
        }
    //estimate the parameters
    cv::Mat foregrrd;
    foregroundDetection(imgOpenCV,foregrrd);
    imgOpenCV = foregrrd;
    cv::Mat bp = backproject(imgOpenCV);
    fingerPos = depth(bp, M, P);
    glove = imgOpenCV.clone();
    handlephysics();
    DrawCube();
    // Any 2D overlays go here.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, (GLdouble)windowWidth, 0, (GLdouble)windowHeight, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    
    glutSwapBuffers();
}

int main(int argc, char** argv)
{
    char glutGamemode[32];
    char cparam_name[] = "C:/Program Files/ARToolKit5/bin/Data/camera_para.dat";
    char vconf[] = "";
    char patt_name[] = "C:/Program Files/ARToolKit5/bin/Data/hiro.patt";
    
    if(OS_Windows){
        strcpy(cparam_name, "C:/Program Files/ARToolKit5/bin/Data/camera_para.dat");
        strcpy(patt_name, "C:/Program Files/ARToolKit5/bin/Data/hiro.patt");
    }
    else{
        strcpy(cparam_name, "Data/camera_para.dat");
        strcpy(patt_name, "Data/hiro.patt");
    }
    
    //
    // Library inits.
    //
    
    if(!OS_Windows){
#if __unix__
        glutInitContextVersion(3, 0);
        glutInitContextProfile(GLUT_CORE_PROFILE);
#endif
    }
    glutInit(&argc, argv);
    
    //
    // Video setup.
    //
    
    if (!setupCamera(cparam_name, vconf, &gCparamLT, &gARHandle, &gAR3DHandle)) {
        ARLOGe("main(): Unable to set up AR camera.\n");
        exit(-10);
    }
    
    
    //
    // Graphics setup.
    //
    
    // Set up GL context(s) for OpenGL to draw into.
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    if (!windowed) {
        if (windowRefresh) sprintf(glutGamemode, "%ix%i:%i@%i", windowWidth, windowHeight, windowDepth, windowRefresh);
        else sprintf(glutGamemode, "%ix%i:%i", windowWidth, windowHeight, windowDepth);
        glutGameModeString(glutGamemode);
        glutEnterGameMode();
    }
    else {
        glutInitWindowSize(windowWidth, windowHeight);
        glutCreateWindow(argv[0]);
    }
    
    // Setup ARgsub_lite library for current OpenGL context.
    if ((gArglSettings = arglSetupForCurrentContext(&(gCparamLT->param), arVideoGetPixelFormat())) == NULL) {
        ARLOGe("main(): arglSetupForCurrentContext() returned error.\n");
        cleanup();
        exit(-4);
    }
    arglSetupDebugMode(gArglSettings, gARHandle);
    arUtilTimerReset();
    
    // Load marker(s).
    if (!setupMarker(patt_name, &gPatt_id, gARHandle, &gARPattHandle)) {
        ARLOGe("main(): Unable to set up AR marker.\n");
        cleanup();
        exit(-2);
    }
    // Register GLUT event-handling callbacks.
    // NB: mainLoop() is registered by Visibility.
    glutDisplayFunc(Display);
    glutReshapeFunc(Reshape);
    glutVisibilityFunc(Visibility);
    glutKeyboardFunc(Keyboard);
    glutKeyboardUpFunc(processNormalKeysUp);
    glutSpecialFunc(processSpecialKeys);
    glutSpecialUpFunc(processSpecialKeysUp);
    glewInit();
    loadObject();
    loadTexture();
    
    sculpt_inst.mod = &sculpt_mod;
    sculpt_inst.mat = &sculpt_mat;
    
    finger_inst.mod = &sculpt_mod;
    finger_inst.mat = &finger_mat;
    
    projectionBackground_inst.mod = &projection_mod;
    projectionBackground_inst.mat = &projectionBackground_mat;
    
    projectionHand_inst.mod = &projection_mod;
    projectionHand_inst.mat = &projectionHand_mat;
    
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    computeHistogram();
    int xsize, ysize;
    arVideoGetSize(&xsize, &ysize);
    int codec =-1;// CV_FOURCC('M', 'J', 'P', 'G');
    writer = cv::VideoWriter ("/Users/seandillon/capture.avi",codec,60, cv::Size(xsize, ysize ));
    if (!writer.isOpened())
    {
        //cout  << "Could not open the output video for write: " << ":'(" << endl;
        return -1;
    }
    glutMainLoop();
    
    return (0);
}

//
// The following functions provide the onscreen help text and mode info.
//

static void print(const char *text, const float x, const float y, int calculateXFromRightEdge, int calculateYFromTopEdge)
{
    int i, len;
    GLfloat x0, y0;
    
    if (!text) return;
    
    if (calculateXFromRightEdge) {
        x0 = windowWidth - x - (float)glutBitmapLength(GLUT_BITMAP_HELVETICA_10, (const unsigned char *)text);
    }
    else {
        x0 = x;
    }
    if (calculateYFromTopEdge) {
        y0 = windowHeight - y - 10.0f;
    }
    else {
        y0 = y;
    }
    glRasterPos2f(x0, y0);
    
    len = (int)strlen(text);
    for (i = 0; i < len; i++) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, text[i]);
}

static void drawBackground(const float width, const float height, const float x, const float y)
{
    GLfloat vertices[4][2];
    
    vertices[0][0] = x; vertices[0][1] = y;
    vertices[1][0] = width + x; vertices[1][1] = y;
    vertices[2][0] = width + x; vertices[2][1] = height + y;
    vertices[3][0] = x; vertices[3][1] = height + y;
    glLoadIdentity();
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glVertexPointer(2, GL_FLOAT, 0, vertices);
    glEnableClientState(GL_VERTEX_ARRAY);
    glColor4f(0.0f, 0.0f, 0.0f, 0.5f);	// 50% transparent black.
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f); // Opaque white.
    //glLineWidth(1.0f);
    //glDrawArrays(GL_LINE_LOOP, 0, 4);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_BLEND);
}

static void printHelpKeys()
{
    int i;
    GLfloat  w, bw, bh;
    const char *helpText[] = {
        "Keys:\n",
        " ? or /        Show/hide this help.",
        " q or [esc]    Quit program.",
        " d             Activate / deactivate debug mode.",
        " m             Toggle display of mode info.",
        " a             Toggle between available threshold modes.",
        " - and +       Switch to manual threshold mode, and adjust threshhold up/down by 5.",
        " x             Change image processing mode.",
        " c             Calulcate frame rate.",
    };
#define helpTextLineCount (sizeof(helpText)/sizeof(char *))
    
    bw = 0.0f;
    for (i = 0; i < helpTextLineCount; i++) {
        w = (float)glutBitmapLength(GLUT_BITMAP_HELVETICA_10, (unsigned char *)helpText[i]);
        if (w > bw) bw = w;
    }
    bh = helpTextLineCount * 10.0f /* character height */ + (helpTextLineCount - 1) * 2.0f /* line spacing */;
    drawBackground(bw, bh, 2.0f, 2.0f);
    
    for (i = 0; i < helpTextLineCount; i++) print(helpText[i], 2.0f, (helpTextLineCount - 1 - i)*12.0f + 2.0f, 0, 0);;
}

static void printMode()
{
    int len, thresh, line, mode, xsize, ysize;
    AR_LABELING_THRESH_MODE threshMode;
    ARdouble tempF;
    char text[256], *text_p;
    
    glColor3ub(255, 255, 255);
    line = 1;
    
    // Image size and processing mode.
    arVideoGetSize(&xsize, &ysize);
    arGetImageProcMode(gARHandle, &mode);
    if (mode == AR_IMAGE_PROC_FRAME_IMAGE) text_p = "full frame";
    else text_p = "even field only";
    snprintf(text, sizeof(text), "Processing %dx%d video frames %s", xsize, ysize, text_p);
    print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
    line++;
    
    // Threshold mode, and threshold, if applicable.
    arGetLabelingThreshMode(gARHandle, &threshMode);
    switch (threshMode) {
        case AR_LABELING_THRESH_MODE_MANUAL: text_p = "MANUAL"; break;
        case AR_LABELING_THRESH_MODE_AUTO_MEDIAN: text_p = "AUTO_MEDIAN"; break;
        case AR_LABELING_THRESH_MODE_AUTO_OTSU: text_p = "AUTO_OTSU"; break;
        case AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE: text_p = "AUTO_ADAPTIVE"; break;
        case AR_LABELING_THRESH_MODE_AUTO_BRACKETING: text_p = "AUTO_BRACKETING"; break;
        default: text_p = "UNKNOWN"; break;
    }
    snprintf(text, sizeof(text), "Threshold mode: %s", text_p);
    if (threshMode != AR_LABELING_THRESH_MODE_AUTO_ADAPTIVE) {
        arGetLabelingThresh(gARHandle, &thresh);
        len = (int)strlen(text);
        snprintf(text + len, sizeof(text) - len, ", thresh=%d", thresh);
    }
    print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
    line++;
    
    // Border size, image processing mode, pattern detection mode.
    arGetBorderSize(gARHandle, &tempF);
    snprintf(text, sizeof(text), "Border: %0.1f%%", tempF*100.0);
    arGetPatternDetectionMode(gARHandle, &mode);
    switch (mode) {
        case AR_TEMPLATE_MATCHING_COLOR: text_p = "Colour template (pattern)"; break;
        case AR_TEMPLATE_MATCHING_MONO: text_p = "Mono template (pattern)"; break;
        case AR_MATRIX_CODE_DETECTION: text_p = "Matrix (barcode)"; break;
        case AR_TEMPLATE_MATCHING_COLOR_AND_MATRIX: text_p = "Colour template + Matrix (2 pass, pattern + barcode)"; break;
        case AR_TEMPLATE_MATCHING_MONO_AND_MATRIX: text_p = "Mono template + Matrix (2 pass, pattern + barcode "; break;
        default: text_p = "UNKNOWN"; break;
    }
    len = (int)strlen(text);
    //snprintf(text + len, sizeof(text) - len, ", Pattern detection mode: %s", text_p);
    print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
    line++;
    
    // Window size.
    snprintf(text, sizeof(text), "Drawing into %dx%d window", windowWidth, windowHeight);
    print(text, 2.0f, (line - 1)*12.0f + 2.0f, 0, 1);
    line++;
    
}

//function taken from http://vgl-ait.org/cvwiki/doku.php?id=gltocv:main
void getScreenShot(IplImage *imgO)
{
    int width = imgO->width;
    int height = imgO->height;
    int hIndex = 0;
    int wIndex = 0;
    int iout, jout;
    
    unsigned char* imageData = (unsigned char*)malloc(width*height * 3);
    IplImage *img = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 3);
    
    glReadPixels(0, 0, width - 1, height - 1, GL_RGB, GL_UNSIGNED_BYTE, imageData);
    
    for (int i = 0; i<width*height * 3; i += 3, wIndex++)
    {
        if (wIndex >= width)
        {
            wIndex = 0;
            hIndex++;
        }
        
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 0] = imageData[i + 2]; // B
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 1] = imageData[i + 1]; // G
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 2] = imageData[i]; // R
        
        // vertically flip the image
        iout = -hIndex + height - 1;
        jout = wIndex;
        
        ((uchar *)(imgO->imageData + iout*imgO->widthStep))[jout*imgO->nChannels + 0] =
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 0];// B
        ((uchar *)(imgO->imageData + iout*imgO->widthStep))[jout*imgO->nChannels + 1] =
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 1];// G
        ((uchar *)(imgO->imageData + iout*imgO->widthStep))[jout*imgO->nChannels + 2] =
        ((uchar *)(img->imageData + hIndex*img->widthStep))[wIndex*img->nChannels + 2];// R
    }
    
    free(imageData);
    cvReleaseImage(&img);
}
