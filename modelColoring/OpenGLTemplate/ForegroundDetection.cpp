//
//  ForegroundDetection.cpp
//  ARToolKit5
//
//  Created by Sean Dillon on 4/28/17.
//
//

#include "ForegroundDetection.h"

using namespace cv;

Ptr<BackgroundSubtractor> pMOG2 = createBackgroundSubtractorMOG2();

void foregroundDetection(Mat& frame,Mat& output)
{
    Mat result;
    pMOG2->apply(frame, result);
    frame.copyTo(output,result);
}
