#ifndef _VBO_H_
#define _VBO_H_
#include <glm/glm.hpp>
#include <vector>
#include "model.h"
namespace buffers {
	void Load(model& mod);
	void Bind(model& mod);
	void Clear();
	void RenderTriangles(int numVertices);
}
#endif