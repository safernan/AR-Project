//OBJ loader based on online tutorial code
#ifndef _AUXILARYIO_H
#define AUXILARYIO_H
#ifdef __unix__ 
    #define OS_Windows 0
#elif defined(_WIN32) || defined(WIN32)     /* _Win32 is usually defined by compilers targeting 32 or   64 bit Windows systems */
    #define OS_Windows 1
    #include <windows.h>
#endif
#include <vector>
#include <glm/glm.hpp>

bool loadOBJ(const char * path, std::vector <glm::vec3> & out_vertices, std::vector <glm::vec3> & out_normals,std::vector <glm::vec2> & out_uvs);
#endif
