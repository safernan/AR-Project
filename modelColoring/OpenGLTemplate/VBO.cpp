#include <GL/glew.h>
#include "VBO.h"
#include <iostream>
void buffers::Load(model & mod)
{

	if (mod.vertices.size() > 0) {
		if (mod.verticesID == -1) {
			glGenBuffers(1, &mod.verticesID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.verticesID);
		glBufferData(GL_ARRAY_BUFFER, mod.vertices.size() * sizeof(glm::vec3), &mod.vertices[0], GL_STATIC_DRAW);
	}
	if (mod.normals.size() > 0) {
		if (mod.normalsID == -1) {
			glGenBuffers(1, &mod.normalsID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.normalsID);
		glBufferData(GL_ARRAY_BUFFER, mod.normals.size() * sizeof(glm::vec3), &mod.normals[0], GL_STATIC_DRAW);
	}
	if (mod.UVs.size() > 0) {
		if (mod.UVsID == -1) {
			glGenBuffers(1, &mod.UVsID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.UVsID);
		glBufferData(GL_ARRAY_BUFFER, mod.UVs.size() * sizeof(glm::vec2), &mod.UVs[0], GL_STATIC_DRAW);
	}
	if (mod.tangents.size() > 0) {
		if (mod.tangentsID == -1) {
			glGenBuffers(1, &mod.tangentsID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.tangentsID);
		glBufferData(GL_ARRAY_BUFFER, mod.tangents.size() * sizeof(glm::vec3), &mod.tangents[0], GL_STATIC_DRAW);
	}
	if (mod.bitangents.size() > 0) {
		if (mod.bitangetsID == -1) {
			glGenBuffers(1, &mod.bitangetsID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.bitangetsID);
		glBufferData(GL_ARRAY_BUFFER, mod.bitangents.size() * sizeof(glm::vec3), &mod.bitangents[0], GL_STATIC_DRAW);
	}
	//extra attributes
	if (mod.actives.size() > 0) {
		if (mod.activedID == -1) {
			glGenBuffers(1, &mod.activedID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, mod.activedID);
		glBufferData(GL_ARRAY_BUFFER, mod.actives.size() * sizeof(float), &mod.actives[0], GL_STATIC_DRAW);
	}
}

void buffers::RenderTriangles(int verticesNum) {
	//render the vertices as a bunch of consecutive triangles
	glDrawArrays(GL_TRIANGLES, 0, verticesNum);
}

void buffers::Bind(model & mod)
{

//std::cout<<mod.verticesID<<","<<mod.normalsID<<","<<mod.UVsID<<","<<mod.bitangetsID<<std::endl;
//std::cout<<"1"<<std::endl;
	if (mod.verticesID == -1) {
		glDisableVertexArrayAttrib(0, 0);
	}
	else {
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, mod.verticesID);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
//td::cout<<"2"<<std::endl;
	if (mod.normalsID == -1) {
		glDisableVertexArrayAttrib(1, 0);
	}
	else {
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, mod.normalsID);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

//std::cout<<"3"<<std::endl;
	if (mod.UVsID == -1) {
		glDisableVertexArrayAttrib(2, 0);
	}
	else {
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, mod.UVsID);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

//std::cout<<"'"<<std::endl;
	if (mod.tangentsID == -1) {
		//glDisableVertexArrayAttrib(3, 0);
	}
	else {
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, mod.tangentsID);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

//std::cout<<"5"<<std::endl;
	if (mod.bitangetsID == -1) {
		//glDisableVertexArrayAttrib(4, 0);
	}
	else {
		glEnableVertexAttribArray(4);
		glBindBuffer(GL_ARRAY_BUFFER, mod.bitangetsID);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

//std::cout<<"6"<<std::endl;
	if (mod.activedID == -1) {
		//glDisableVertexArrayAttrib(5, 0);
	}
	else {
		glEnableVertexAttribArray(5);
		glBindBuffer(GL_ARRAY_BUFFER, mod.activedID);
		glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
}

void buffers::Clear()
{
	glDisableVertexArrayAttrib(0, 0);
	glDisableVertexArrayAttrib(1, 0);
	glDisableVertexArrayAttrib(2, 0);
	glDisableVertexArrayAttrib(3, 0);
	glDisableVertexArrayAttrib(4, 0);
	glDisableVertexArrayAttrib(5, 0);
}
