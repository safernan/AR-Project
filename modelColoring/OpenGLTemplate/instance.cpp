#include "instance.h"
#include "global.h"
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/string_cast.hpp> 
#include <glm/gtx/matrix_decompose.hpp>
#include "VBO.h"
#include <iostream>

glm::mat4 instance::rotate(GLfloat rotX, GLfloat rotY, GLfloat rotZ)
{
	transform=glm::rotate(transform, rotX*0.0174533f, glm::vec3(1,0, 0));
	transform=glm::rotate(transform, rotY*0.0174533f, glm::vec3(0, 1, 0));
	transform=glm::rotate(transform, rotZ*0.0174533f, glm::vec3(0, 0, 1));
	return transform;
}

glm::mat4 instance::scale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ)
{
	transform = glm::scale(transform, glm::vec3(scaleX, scaleY, scaleZ));
	return transform;
}

glm::mat4 instance::rotateAround(GLfloat rotX, GLfloat rotY, GLfloat rotZ, glm::vec3 pivot)
{
	//glm::mat4 transformation; // your transformation matrix.
	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	glm::decompose(transform, scale, rotation, translation, skew, perspective);
	//
	glm::vec3 offset = pivot - translation;
	transform=glm::translate(transform,offset);
	transform=glm::rotate(transform, rotY, glm::vec3(0, 1, 0));
	transform = glm::translate(transform,-offset);
	return transform;
}

glm::mat4 instance::translate(GLfloat transX, GLfloat transY, GLfloat transZ)
{
	transform=glm::translate(transform, glm::vec3(transX, transY, transZ));
	return transform;
}

void instance::BindMaterial(){
	if (mat == NULL)return;
	glUseProgram(mat->ShaderID);
	mat->BindTextures();
	mat->BindValues();
	glm::mat4 MV = camera::ViewMatrix *transform;
	glm::mat3 MV3X3 = glm::mat3(MV);
	glm::mat4 MVP = camera::ProjectionMatrix * camera::ViewMatrix * transform;
	glm::mat4 MVPNoTranslation = camera::ProjectionMatrix*camera::ViewMatrixNoTranslation*transform;
	glm::mat4 NM = glm::transpose(glm::inverse(MV));
	GLuint MLocation = glGetUniformLocation(mat->ShaderID, "M");
	GLuint MVLocation = glGetUniformLocation(mat->ShaderID, "MV");
	GLuint MV3X3Location = glGetUniformLocation(mat->ShaderID, "MV3X3");
 	GLuint MVPLocation = glGetUniformLocation(mat->ShaderID, "MVP");
	GLuint NMLocation = glGetUniformLocation(mat->ShaderID, "NM");
	GLuint MVPNoTranslationLocation = glGetUniformLocation(mat->ShaderID, "MVPNoTranslation");

	glUniformMatrix4fv(MLocation,1,GL_FALSE, &transform[0][0]);
	glUniformMatrix4fv(MVLocation, 1, GL_FALSE, &MV[0][0]);
	glUniformMatrix3fv(MV3X3Location, 1, GL_FALSE, &MV3X3[0][0]);
	glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(NMLocation, 1, GL_FALSE, &NM[0][0]);
	glUniformMatrix4fv(MVPNoTranslationLocation, 1, GL_FALSE, &MVPNoTranslation[0][0]);
	
}

void instance::BindModel()
{
	if (mod == NULL)return;
	buffers::Bind(*mod);
}

void instance::Render()
{
	BindMaterial();
	BindModel();
	buffers::RenderTriangles(mod->vertices.size());
	//buffers::Clear();
}

void instance::RenderWithoutMaterials() {
	BindModel();
	GLuint MLocation = glGetUniformLocation(lights::light0.depthMapShaderID, "M");
	glUniformMatrix4fv(MLocation, 1, GL_FALSE, &transform[0][0]);
	buffers::RenderTriangles(mod->vertices.size());
}
