//
//  ForegroundDetection.h
//  ARToolKit5
//
//  Created by Sean Dillon on 4/28/17.
//
//

#ifndef ARToolKit5_ForegroundDetection_h
#define ARToolKit5_ForegroundDetection_h


#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>

void foregroundDetection(cv::Mat& frame,cv::Mat& result);

#endif
