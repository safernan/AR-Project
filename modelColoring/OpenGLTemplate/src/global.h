#ifndef  _GLOBAL_H
#define _GLOBAL_H

#include <glm/matrix.hpp>
#include <GL\glew.h>
#include "light.h"
namespace camera{
	extern glm::mat4 ViewMatrix;
	extern glm::mat4 ProjectionMatrix;
	extern GLfloat ratio;
	extern GLfloat FOV;
	extern glm::vec3 position;
	extern glm::mat4 ViewMatrixNoTranslation;
}

namespace viewport {
	extern int width;
	extern int height;
}

namespace lights {
	 extern light light0;
	 extern light light1;
}

namespace controls {
	//alpharithmetics
	extern GLboolean Key_A;
	extern GLboolean Key_D;
	extern GLboolean Key_W;
	extern GLboolean Key_S;
	extern GLboolean Key_I;
	extern GLboolean Key_K;
	extern GLboolean Key_J;
	extern GLboolean Key_L;
	extern GLboolean Key_U;
	extern GLboolean Key_O;
	//specials
	extern GLboolean Key_Left;
	extern GLboolean Key_Right;
}

namespace time {
	extern GLfloat previousFrameTime;
	extern GLfloat currentFrameTime;
	extern GLfloat deltaTime;
}

#endif