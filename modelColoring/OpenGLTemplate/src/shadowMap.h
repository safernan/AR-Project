#ifndef _SHADOWMAP_H
#define _SHADOWMAP_H
struct shadowMap {
	//add shadowmap information here
	int width = 1024;
	int height = 1024;
	unsigned int depthMapID = -1;
	unsigned int backfaceDepthMapID = -1;
	unsigned int colorMapID = -1;
	unsigned int frameBufferID = -1;
};
#endif
