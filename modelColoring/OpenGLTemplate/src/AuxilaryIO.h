//OBJ loader based on online tutorial code
#ifndef _AUXILARYIO_H
#define AUXILARYIO_H
#include <windows.h>
#include <vector>
#include <glm\glm.hpp>

bool loadOBJ(const char * path, std::vector <glm::vec3> & out_vertices, std::vector <glm::vec3> & out_normals,std::vector <glm::vec2> & out_uvs);
#endif