#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
	vec3 RealFragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
	vec3 Tangent;
	vec3 Bitangent;
} fs_in;

uniform sampler2D diffuseMap;
uniform sampler2D depthMap;
uniform sampler2D backfaceDepthMap;
uniform sampler2D colorMap;
uniform sampler2D normalMap;

uniform vec3 LP;
uniform vec3 viewPos;

int kernelSize=1;
float lightEmission=100;

uniform int choice =0;


float ShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(depthMap, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
	float bias=0.005f;
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(depthMap, 0);
	for(int x = -kernelSize; x <= kernelSize; ++x)
	{
	 for(int y = -kernelSize; y <= kernelSize; ++y)
	   {
			float pcfDepth = texture(depthMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /= (2*kernelSize+1)*(2*kernelSize+1);

    return shadow;
}

float BackShadowCalculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(backfaceDepthMap, projCoords.xy).r;
    // Get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // Check whether current frag pos is in shadow
	float bias=0.005f;
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(backfaceDepthMap, 0);
	for(int x = -kernelSize; x <= kernelSize; ++x)
	{
	 for(int y = -kernelSize; y <= kernelSize; ++y)
	   {
			float pcfDepth = texture(backfaceDepthMap, projCoords.xy + vec2(x, y) * texelSize).r; 
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;        
		}    
	}
	shadow /=(2*kernelSize+1)*(2*kernelSize+1);

    return shadow;
}

float ThicknessCalculation(vec4 fragPosLightSpace)
{
 // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float backDepth = texture(backfaceDepthMap, projCoords.xy).r; 
	float frontDepth = texture(depthMap, projCoords.xy).r;
	return (backDepth-frontDepth);

}
vec4 ShadowColorCalculation(vec4 fragPosLightSpace)
{
// perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // Transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // Get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
	float bias=0.005f;
	vec4 color= vec4(0.0,0.0,0.0,0.0);
	vec2 texelSize = 1.0 / textureSize(colorMap, 0);
	for(int x = -kernelSize; x <= kernelSize; ++x)
	{
	 for(int y = -kernelSize; y <= kernelSize; ++y)
	   {
			vec4 pcfColor = texture(colorMap, projCoords.xy + vec2(x, y) * texelSize); 
			color += pcfColor;      
		}    
	}
	color /= (2*kernelSize+1)*(2*kernelSize+1);
	return color;
}

void main()
{           
    vec3 color = texture(diffuseMap, fs_in.TexCoords).rgb;
	vec3 normal= normalize(texture(normalMap,fs_in.TexCoords).rgb*2.0-1.0);
	mat3 TBN = transpose(mat3(fs_in.Tangent,fs_in.Bitangent,fs_in.Normal));
    vec3 lightColor = vec3(0.5,0.5,0);
	vec4 shadowColor = ShadowColorCalculation( fs_in.FragPosLightSpace); 
    // Diffuse
  	vec3 light = TBN*normalize(LP- fs_in.RealFragPos);
	vec3 eyes = TBN* normalize(-fs_in.RealFragPos);
	vec3 halfv = normalize(light + eyes);  
	vec3 diffuse= max(dot(light,halfv), 0.0)*0.015*color;
    // Specular  
    float spec = pow(max(dot(normal, halfv), 0.0), 64.0)*0.65;
    vec3 specular = spec * color;    
    // Calculate shadow
    float shadow = ShadowCalculation(fs_in.FragPosLightSpace)*BackShadowCalculation(fs_in.FragPosLightSpace);
	  // Ambient
    vec3 ambient = color*0.25;
    float thickness=ThicknessCalculation(fs_in.FragPosLightSpace)*100f+0.1f;
	vec3 lighting = ambient+diffuse+specular;
	//this branching is done for the demo. Normaly, only the last case is used.
   if(choice==0){
		FragColor = vec4(lighting,1.0);
	}
	else if(choice==1){ 
		FragColor = thickness*vec4(lighting,1.0)+vec4(0,0,0,1);
	}
	else if(choice==2){
		FragColor =  shadow*thickness*vec4(lighting,1.0)+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	}
	else if(choice==3){
		FragColor =  shadow*thickness*vec4(lighting,1.0)*50+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	}
	else if(choice==4){
		FragColor =  shadow*thickness*vec4(lighting,1.0)*50*shadowColor+(1-shadow)*vec4(lighting,0.0)+vec4(0,0,0,1);
	}
	else{
		FragColor =shadow*thickness*vec4(lighting,1.0)*lightEmission*shadowColor+(1-shadow)*vec4(ambient,1)+vec4(0,0,0,1);
	}

}