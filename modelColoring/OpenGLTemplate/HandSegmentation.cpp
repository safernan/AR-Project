#include "HandSegmentation.h"
void handSegmentation(cv::Mat& cameraImage,cv::Mat& handImage,
                      int* mChannelNumbers,cv::MatND mHistogram,const float** channel_ranges){
    
    cv::Mat hls_image=cameraImage.clone();
   // cv::cvtColor(cameraImage, hls_image, CV_BGRA2BGR);
    cv::cvtColor(hls_image,hls_image, CV_BGR2HLS);
    cv::Mat image2 = hls_image;
    cv::Mat result = image2.clone();
    cv::calcBackProject(&image2, 1, mChannelNumbers, mHistogram, result, channel_ranges, 255.0);
    cv::Mat back_projection_probabilities_display, binary_image;
    cvtColor(result, back_projection_probabilities_display, CV_GRAY2BGR);
    cv::threshold(result, binary_image, 0.0, 255.0, cv::THRESH_OTSU);
    
    cv::Mat closed_image;
    cv::Mat five_by_five_element(15, 15, CV_8U, cv::Scalar(1));
    morphologyEx(binary_image, closed_image, cv::MORPH_CLOSE, five_by_five_element);
    
    handImage = cameraImage.clone();
    
    for (int row = 0; row < handImage.rows; row++)
    {
        for (int col = 0; col < handImage.cols; col++)
        {
            uchar black_pixel = closed_image.at<uchar>(row, col);
            if (black_pixel!=0)
            {
                handImage.at<cv::Vec4b>(row, col)[0] = cameraImage.at<cv::Vec4b>(row, col)[0];
                handImage.at<cv::Vec4b>(row, col)[1] = cameraImage.at<cv::Vec4b>(row, col)[1];
                handImage.at<cv::Vec4b>(row, col)[2] = cameraImage.at<cv::Vec4b>(row, col)[2];
            }
            else
            {
                handImage.at<cv::Vec4b>(row, col)[0] = 0;
                handImage.at<cv::Vec4b>(row, col)[1] = 0;
                handImage.at<cv::Vec4b>(row, col)[2] = 0;
            }
        }
    }

}