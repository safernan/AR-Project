#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
void handSegmentation(cv::Mat& cameraImage,cv::Mat& handImage,int* mChannelNumbers,
                           cv::MatND mHistogram,const float** channel_ranges);
//void background