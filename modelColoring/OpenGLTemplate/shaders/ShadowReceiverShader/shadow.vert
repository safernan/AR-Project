#version 330 core
layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;

out VS_OUT {
    vec3 FragPos;
	vec3 RealFragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace;
	vec3 Tangent;
	vec3 Bitangent;
} vs_out;

uniform mat4 MVP;
uniform mat4 M;
uniform mat4 MV;
uniform mat3 MV3X3;
uniform mat4 lightSpaceMatrix;

void main()
{
    gl_Position = MVP*vec4(pos, 1.0f);
    vs_out.FragPos = vec3(M * vec4(pos, 1.0));
	vs_out.RealFragPos =  (M* vec4(pos.x,pos.y,pos.z,1)).xyz;
    vs_out.Normal = transpose(inverse(mat3(M))) * normal;
    vs_out.TexCoords = uv;
    vs_out.FragPosLightSpace = lightSpaceMatrix * vec4(vs_out.FragPos, 1.0);
	vs_out.Tangent = MV3X3 * normalize(tangent);
	vs_out.Bitangent = MV3X3*normalize(bitangent);
}