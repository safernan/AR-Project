#version 450

uniform samplerCube skyboxMap;
uniform float alphaValue;

layout(location = 0) out vec4 fragment_color;

in vec3 Reflect;

void main (void){
	vec3 reflectColor = vec3(texture(skyboxMap, Reflect));
	fragment_color = vec4(reflectColor,alphaValue);
}