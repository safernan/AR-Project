varying vec3 normal,view; 
void main (void)  
{  
   vec3 light = normalize(gl_LightSource[0].position.xyz - view);
   vec3 eyes =     
   float diff= max(dot(light,normal), 0.0);
   float spec=0.0;
   if(diff>0.0)
	spec = pow(max(dot(halff, normal), 0.0),2);   
   gl_FragColor = vec4(0,0,1,1)+ clamp(diff*vec4(1,0,0,1),0.0,1.0)+clamp(spec*vec4(1,1,1,1),0.0,1.0);     
}