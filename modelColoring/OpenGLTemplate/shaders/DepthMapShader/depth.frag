#version 330 core

layout(location=0) out vec3 color;

void main()
{             
     gl_FragDepth = gl_FragCoord.z;
}  