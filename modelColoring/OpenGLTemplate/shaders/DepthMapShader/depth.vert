#version 330 core
layout (location = 0) in vec3 pos;

uniform mat4 lightSpaceMatrix;
uniform mat4 M;

out vec3 position;

void main()
{
	position = (lightSpaceMatrix*M*vec4(pos,1.0f)).xyz;
    gl_Position = lightSpaceMatrix * M * vec4(pos, 1.0f);
}  