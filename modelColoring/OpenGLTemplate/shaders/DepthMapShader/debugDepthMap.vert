#version 330 core

uniform mat4 MVP;
layout(location = 0) in vec3 pos;
layout(location = 2) in vec2 uv;

out vec2 var_uv;

void main()
{
	var_uv = uv;
	gl_Position = MVP * vec4(pos,1); 
}  