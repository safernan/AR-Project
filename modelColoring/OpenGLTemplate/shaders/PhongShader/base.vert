
varying vec3 normal,view;
varying vec2 vTexCoord;
void main(void)  
{     
   view = vec3(gl_ModelViewMatrix * gl_Vertex);       
   normal = normalize(gl_NormalMatrix * gl_Normal);
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    vTexCoord = gl_MultiTexCoord0.xy;

}