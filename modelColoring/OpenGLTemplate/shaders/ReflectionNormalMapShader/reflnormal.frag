#version 450


uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform vec3 LP;

uniform samplerCube skyboxMap;

uniform float alphaValue;

layout(location = 0) out vec4 fragment_color;

in vec3 var_pos;
in vec3 var_normal;
in vec2 var_uv;
in vec3 var_tangent;
in vec3 var_bitangent;

void main (void){
    mat3 TBN = transpose(mat3(var_tangent,var_bitangent,var_normal));
	vec3 normalTexel = TBN*normalize(texture(normalMap,var_uv).rgb*2.0-1.0);
	vec3 reflection = reflect(normalize(var_pos),normalTexel);
	vec4 reflectionColor = texture(skyboxMap,reflection);
	fragment_color = vec4(reflectionColor.rgb,alphaValue);
}