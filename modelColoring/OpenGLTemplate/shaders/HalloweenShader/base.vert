varying vec3 normal,view;
varying vec2 vTexCoord;
uniform float time;
void main(void)  
{     
   view = vec3(gl_ModelViewMatrix * gl_Vertex);       
   normal = normalize(gl_NormalMatrix * gl_Normal);
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex+vec4(normal[0],normal[1],normal[2]*0,0)*sin(time*0.5)*0.5;
    vTexCoord = gl_MultiTexCoord0.xy;

}