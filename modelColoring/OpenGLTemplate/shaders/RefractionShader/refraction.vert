#version 450

uniform mat4 M;
uniform mat4 MV;
uniform mat4 MVP;
uniform mat4 NM;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

const float EtaR = 0.97;
const float EtaG = 0.98; // Ratio of indices of refraction
const float EtaB = 0.99;

out vec3 RefractR,RefractG,RefractB;

void main(void){
 gl_Position = MVP * vec4(pos,1);
  vec3 I = ((M*vec4(pos,1)).xyz);
  RefractR = refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaR);
  RefractG = refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaG);
  RefractB = refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaB);
}