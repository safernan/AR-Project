#version 120

precision mediump float;

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform vec3 LP;
uniform float alphaValue;

layout(location = 0) out vec4 fragment_color;

in vec3 var_pos;
in vec3 var_normal;
in vec2 var_uv;
in vec3 var_tangent;
in vec3 var_bitangent;
in float var_active;

void main (void){
    mat3 TBN = transpose(mat3(var_tangent,var_bitangent,var_normal));
 	vec4 diffuseTexel = texture(diffuseMap,var_uv);
	vec3 normalTexel = normalize(texture(normalMap,var_uv).rgb*2.0-1.0);
	vec3 light =TBN*normalize(LP- var_pos);
	vec3 eyes = TBN* normalize(-var_pos);
	vec3 halfv = normalize(light + eyes);  
	float diff= max(dot(light,halfv), 0.0);
	fragment_color = //var_active*vec4(1,0,0,1)+(1-var_active)*vec4(0,1,0,1);
	vec4(diffuseTexel.rgb,1.0f); //vec4(diff*diffuseTexel.rgb,0.2f);
}
