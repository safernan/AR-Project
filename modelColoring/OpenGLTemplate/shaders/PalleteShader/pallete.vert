#version 120

uniform mat4 MV;
uniform mat4 MVP;
uniform mat3 MV3X3;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 tangent;
layout(location = 4) in vec3 bitangent;
layout(location = 5) in float actives;

out vec3 var_pos;
out vec3 var_normal;
out vec2 var_uv;
out vec3 var_tangent;
out vec3 var_bitangent;
out float var_active;

void main(void){
  gl_Position = MVP * vec4(pos.x,pos.y,pos.z,1);
  var_pos = (MV* vec4(pos.x,pos.y,pos.z,1)).xyz;
  var_normal = MV3X3*normalize(normal);
  var_uv.x = uv.x; var_uv.y=uv.y;
  var_tangent = MV3X3 * normalize(tangent);
  var_bitangent = MV3X3*normalize(bitangent);
  var_active = actives;
}
