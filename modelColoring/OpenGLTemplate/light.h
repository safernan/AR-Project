#ifndef  _LIGHT_H
#define _LIGHT_H

#define ORTHO 0
#define PERSP 1


#include "shadowMap.h"
#include <glm/vec3.hpp>
#include <glm/matrix.hpp>
#include <GL/glew.h>

struct light {
	glm::vec3 orientation;
	glm::vec3 position;
	glm::mat4 lightSpace;
	glm::mat4 lightView;
	shadowMap map;
	GLuint depthMapShaderID=-1;
	void StartFirstPass();
	void EndFirstPass();
	void StartSecondPass();
	void EndSecondPass();
	void StartThirdPass();
	void EndThirdPass();
	void BindShadowMap();

	void SetLightProjectionPerspective();
	void SetLightProjectionOrthographic();

	int lightType = ORTHO;
};

#endif

